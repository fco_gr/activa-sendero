<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        body{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 0.875em;
        }
        table {
            width: 100%;
        }
        th, td {
            padding: 8px;
            text-align: left;
        }
        th, td {
            border-bottom: 1px solid #ddd;
        }
    </style>
    <title>Document</title>
</head>
<body>
    <p>
        {{ $contacto['mensaje'] }}
    </p>

    <hr>

    <p>
        <strong>Detalle evaluación:</strong>
    </p>

    <table>
        <tr>
            <th>Fecha Evaluación</th>
            <td>{{ date_format(date_create($contacto['dato']['fecha_medicion']), 'd-m-Y') }}</td>
        </tr>
        <tr>
            <th>Viaje</th>
            <td>{{ $contacto['dato']['estudio']['nom_estudio'] }}</td>
        </tr>
        <tr>
            <th>Rut</th>
            <td>{{ $contacto['dato']['RUT_CLNT_TIT'] }}</td>
        </tr>
        <tr>
            <th>Nombre</th>
            <td>{{ $contacto['dato']['NMB_CLNT_TIT'] }}</td>
        </tr>

        <tr>
            <th>Teléfono</th>
            <td>
                {{ $contacto['dato']['COD_AREA_FONO_FIJO'] ? "(" . $contacto['dato']['COD_AREA_FONO_FIJO'].')' :  ""}} &nbsp;&nbsp;
                {{ $contacto['dato']['FONO_FIJO'] ? $contacto['dato']['FONO_FIJO'] :  ""}}
            
            </td>
        </tr>

        <tr>
            <th>Celular</th>
            <td>
                {{ $contacto['dato']['FONO_CELULAR'] ? $contacto['dato']['FONO_CELULAR'] :  ""}}
            </td>
        </tr>

        <tr>
            <th>Mail</th>
            <td>
                {{ $contacto['dato']['EMAIL_CLIE'] ? $contacto['dato']['EMAIL_CLIE'] :  ""}}
            </td>
        </tr>

        <tr>
            <th>Tipo Necesidad</th>
            <td>
                {{ $contacto['dato']['TIPO_NECES_OP'] ? $contacto['dato']['TIPO_NECES_OP'] :  ""}}
            </td>
        </tr>

        <tr>
            <th>Producto</th>
            <td>
                {{ $contacto['dato']['TIPO_PRODUCTO'] ? $contacto['dato']['TIPO_PRODUCTO'] :  ""}}
            </td>
        </tr>

        <tr>
            <th>Fracción</th>
            <td>
                {{ $contacto['dato']['FRACCION'] ? $contacto['dato']['FRACCION'] :  ""}}
            </td>
        </tr>

        <tr>
            <th>Parque</th>
            <td>
                {{ $contacto['dato']['PARQUE_FISICO'] ? $contacto['dato']['PARQUE_FISICO'] :  ""}}
            </td>
        </tr>

        <tr>
            <th>Satisfacción Global</th>
            <td>
                {{ $contacto['dato']['P14'] ? $contacto['dato']['P14'] :  ""}}
            </td>
        </tr>

        <tr>
            <th>Qué sugeriría</th>
            <td>
                {{ $contacto['dato']['P14_1'] ? $contacto['dato']['P14_1'] :  ""}}
            </td>
        </tr>

        <tr>
            <th>Algún inconveniente</th>
            <td>
                {{ $contacto['dato']['P15'] && $contacto['dato']['P15'] == 1 ? "Si" :  "No"}}
            </td>
        </tr>

        <tr>
            <th>¿Cuál fue el inconveniente?</th>
            <td>
                {{ $contacto['dato']['P16'] ? $contacto['dato']['P16']  : ""}}
            </td>
        </tr>

        <tr>
            <th>¿Fue solucionado?</th>
            <td>
                {{ $contacto['dato']['P17'] }}
                @if ($contacto['dato']['P17'] == 1)
                    SI, satisfactoriamente
                @elseif ($contacto['dato']['P17'] == 2)
                    SI, con una respuesta medianamente Satisfactoria
                @elseif ($contacto['dato']['P17'] == 3)
                    NO, La respuesta no solucionó el problema
                @elseif ($contacto['dato']['P17'] == 4)
                    NO comuniqué el problema
                @endif
                
            </td>
        </tr>
        
    </table>

</body>
</html>