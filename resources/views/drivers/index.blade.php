@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    Drivers
@endsection

@section('content')
<div class="row">
    <drivers></drivers>
</div>
    
@endsection