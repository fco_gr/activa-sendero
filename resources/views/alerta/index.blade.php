@extends('layouts.admin')

@section('titulo')
<div class="text-primary">
    <i class="fas fa-bell"></i>
    Alertas
</div>
    
@endsection

@section('content')
    <div class="row">
        <alerta></alerta>
    </div>
@endsection