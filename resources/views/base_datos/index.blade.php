@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <div class="text-primary">
        <i class="fas fa-database"></i>
        Descarga de Bases de datos
    </div>
@endsection

@section('content')
<div class="row">
    <base-datos></base-datos>
</div>
    
@endsection