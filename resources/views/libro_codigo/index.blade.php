@extends('layouts.admin')

@section('titulo')
<div class="text-primary">
    <i class="fas fa-book"></i>
    Libro de Códigos
</div>
    
@endsection

@section('content')
    <div class="row">
        <libro_codigo></libro_codigo>
    </div>
@endsection