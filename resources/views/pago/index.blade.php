@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <div class="text-primary">
        <i class="far fa-credit-card"></i>
        Pago
    </div>
@endsection

@section('content')
<div class="row">
    <pago></pago>
</div>
    
@endsection