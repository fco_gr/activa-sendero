@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <div class="text-primary">
        <i class="fas fa-file-invoice"></i>
        Información Recibida
    </div>
@endsection

@section('content')
<div class="row">
    <informacion_recibida></informacion_recibida>
</div>
    
@endsection