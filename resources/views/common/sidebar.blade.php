<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                {{-- <div class="sb-sidenav-menu-heading">Core</div> --}}
                @can('isAdminOrUserOrAlerta')
                    <a class="{{ request()->is('evolutivo') ? "nav-link active" : "nav-link" }}" href="{{ route('evolutivo.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-chart-line"></i>
                        </div>
                        Evolutivo
                    </a>
                    
                    <a class="{{ request()->is('nps') ? "nav-link active" : "nav-link" }}"  href="{{ route('nps.index') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                        NPS
                    </a>
                    <a class="{{ request()->is('drivers') ? "nav-link active" : "nav-link" }}" href="{{ route('drivers.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-cart-arrow-down"></i>
                        </div>
                        Drivers
                    </a>
                    <a class="{{ request()->is('comparativo_parques') ? "nav-link active" : "nav-link" }}" href="{{ route('comparativo-parques.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-compress-alt"></i>
                        </div>
                        Comparativo Parques
                    </a>
                    <a class="{{ request()->is('imagen') ? "nav-link active" : "nav-link" }}" href="{{ route('imagen.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-eye"></i>
                        </div>
                        Imagen
                    </a>
                    <a class="{{ request()->is('satisfaccion_global') ? "nav-link active" : "nav-link" }}" href="{{ route('satisfaccion-global.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="far fa-thumbs-up"></i>
                        </div>
                        Satisfacción Global
                    </a>
                    
                    <a class="{{ request()->is('venta') ? "nav-link active" : "nav-link" }}" href="{{ route('venta.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-shopping-cart"></i>
                        </div>
                        Venta
                    </a>
                    <a class="{{ request()->is('acompanamiento') ? "nav-link active" : "nav-link" }}" href="{{ route('acompanamiento.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-friends"></i>
                        </div>
                        Acompañamiento
                    </a>
                    {{-- <a class="{{ request()->is('visita_y_pago') ? "nav-link active" : "nav-link" }}" href="{{ route('visita_y_pago.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="far fa-credit-card"></i>
                        </div>
                        Visitas y Pago
                    </a> --}}
                    <a class="{{ request()->is('visita') ? "nav-link active" : "nav-link" }}" href="{{ route('visita.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-walking"></i>
                        </div>
                        Visita
                    </a>
                    <a class="{{ request()->is('pago') ? "nav-link active" : "nav-link" }}" href="{{ route('pago.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="far fa-credit-card"></i>
                        </div>
                        Pago
                    </a>
                    <a class="{{ request()->is('reclamo') ? "nav-link active" : "nav-link" }}" href="{{ route('reclamo.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-exclamation-triangle"></i>
                        </div>
                        Reclamos
                    </a>
                    
                    <a class="{{ request()->is('problema') ? "nav-link active" : "nav-link" }}" class="nav-link" href="{{ route('problema.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-times-circle"></i>
                        </div>
                        Problemas
                    </a>

                    <a class="{{ request()->is('informacion_recibida') ? "nav-link active" : "nav-link" }}" class="nav-link" href="{{ route('informacion_recibida.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-file-invoice"></i>
                        </div>
                        Información Recibida
                    </a>
                @endcan
                
                @can('isAdminOrAlerta')
                    <a class="{{ request()->is('alertas') ? "nav-link active" : "nav-link" }}" class="nav-link" href="{{ route('alertas.index') }}">
                        <div class="sb-nav-link-icon">
                            {{-- <i class="fas fa-file-invoice"></i> --}}
                            <i class="fas fa-bell"></i>
                        </div>
                        Alertas
                    </a>
                    
                @endcan

                <a class="{{ request()->is('base_datos') ? "nav-link active" : "nav-link" }}" class="nav-link" href="{{ route('base_datos.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-database"></i>
                    </div>
                    Descarga BD
                </a>

                @can('isAdminOrCodificador')
                    <a class="{{ request()->is('codificacion') ? "nav-link active" : "nav-link" }}" href="{{ route('codificacion.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-walking"></i>
                        </div>
                        Codificación
                    </a>

                    <a class="{{ request()->is('libro_codigos') ? "nav-link active" : "nav-link" }}" href="{{ route('libro_codigo.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-book"></i>
                        </div>
                        Libro Códigos
                    </a>
                @endcan
                
                @can('isAdmin')
                    <div class="sb-sidenav-menu-heading">Administración</div>
                    <a class="{{ request()->is('users') ? "nav-link active" : "nav-link" }}" href="{{ route('user.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-friends"></i>
                        </div>
                        Usuarios
                    </a>
                    <a class="{{ request()->is('resumen_cawi') ? "nav-link active" : "nav-link" }}" href="{{ route('resumen_cawi.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-friends"></i>
                        </div>
                        Resumen Cawi
                    </a>
                    {{-- <a class="{{ request()->is('dooit*') ? "nav-link" : "nav-link collapsed" }}" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="true" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Dooit
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="{{ request()->is('dooit*') ? "collapse show" : "collapse" }}" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="{{ request()->is('dooit/actualiza') ? "nav-link active" : "nav-link" }}" href="{{ route('dooit') }}">Actualización</a>
                        </nav>
                    </div>
                    <div class="{{ request()->is('dooit*') ? "collapse show" : "collapse" }}" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="{{ request()->is('dooit/datos_detalle') ? "nav-link active" : "nav-link" }}" href="{{ route('datos.detalle') }}">Datos Detalle</a>
                        </nav>
                    </div> --}}
                    
                @endcan
            </div>
        </div>
        
    </nav>
</div>