/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import moment from 'moment';

// import { Form, HasError, AlertError } from 'vform';
// window.Form = Form;

import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '14px'
  })


import Swal from 'sweetalert2'
import Vue from 'vue';
window.swal = Swal;

import Sparkline from 'vue-sparklines'
Vue.use(Sparkline)

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

window.toast = Toast;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('not-found', require('./components/NotFound.vue').default);
Vue.component('users', require('./components/Users.vue').default);
Vue.component('nps', require('./components/Nps/Nps.vue').default);
Vue.component('drivers', require('./components/drivers/Drivers.vue').default);
Vue.component('satisfaccion_global', require('./components/satisfaccion_global/SatisfaccionGlobal.vue').default);
Vue.component('imagen', require('./components/imagen/Imagen.vue').default);
Vue.component('comparativo-parque', require('./components/comparativo_parque/ComparativoParque.vue').default);
Vue.component('evolutivo', require('./components/evolutivo/Evolutivo.vue').default);
Vue.component('venta', require('./components/venta/Venta.vue').default);
Vue.component('acompanamiento', require('./components/acompanamiento/Acompanamiento.vue').default);
Vue.component('visita_y_pago', require('./components/visita_y_pago/VisitaYPago.vue').default);
Vue.component('visita', require('./components/visita/Visita.vue').default);
Vue.component('pago', require('./components/pago/Pago.vue').default);
Vue.component('reclamo', require('./components/reclamo/Reclamo.vue').default);
Vue.component('problema', require('./components/problema/Problema.vue').default);
Vue.component('informacion_recibida', require('./components/informacion_recibida/InformacionRecibida.vue').default);
Vue.component('resumen_cawi', require('./components/resumen_cawi/ResumenCawi.vue').default);
Vue.component('alerta', require('./components/alerta/Alerta.vue').default);


Vue.component('codificacion', require('./components/codificacion/Codificacion.vue').default);
Vue.component('libro_codigo', require('./components/libro_codigo/LibroCodigo.vue').default);

Vue.component('filtro-completo', require('./components/FiltroCompleto.vue').default);

Vue.component('base-datos', require('./components/base_datos/BaseDatos.vue').default);


// Vue.component('graph-line', require('./components/Chart/ChartInLine.vue').default);


// Vue.component('dashboard', require('./components/Dashboard.vue').default);
// Vue.component('benchmark', require('./components/benchmark/Benchmark.vue').default);

// Vue.component('ranking-local', require('./components/ranking_local/RankingLocal.vue').default);

// // Vue.component('example-chart', require('./components/RandomChart.vue').default);
// Vue.component('grafica-component', require('./components/graficos.vue').default);


// Vue.component('filtro-sucursal', require('./components/filtros/FiltroSucursal.vue').default);
// Vue.component('punto-contacto', require('./components/puntoContacto/PuntoContacto.vue').default);

// Vue.component('variedad-por-seccion', require('./components/variedadPorSeccion/VariedadPorSeccion.vue').default);
// Vue.component('tabla-variedad-por-seccion', require('./components/variedadPorSeccion/TablaVariedadPorSeccion.vue').default);


Vue.component('passport-clients', require('./components/passport/Clients.vue').default);
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue').default);
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue').default);

// Filter


Vue.filter('myDate',function(created){
  return moment(created).format('MMMM Do YYYY');
});

Vue.filter('myDateShort', function(fecha){
  return moment(fecha).format('DD-MM-YYYY');
});

Vue.filter('myDateLong', function(fecha){
  return moment(fecha).format('DD-MM-YYYY h:mm:ss');
});


Vue.filter('yesno', value => (value ? '<i class="fas fa-check green"></i>' : '<i class="fas fa-times red"></i>'));

// end Filter

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
