import { Bar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
    extends: Bar,
    mixins: [reactiveProp],
    props: ['chartData', 'options'], 
    mounted () {
        // this.chartData is created in the mixin.
        // If you want to pass options please create a local options object
        // this.renderChart(this.chartData, this.options);
        // console.log(this.chartData);
        
        
        
        this.renderChart({
            labels: this.chartData.labels,
            datasets: this.chartData.datasets,
        }, 
        {
            // options: this.options,
            responsive: true, 
            maintainAspectRatio: false, 
            onClick:this.handle,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        max: 120 // maximum value
                    }
                }]
            },
            hover: {
                animationDuration: 0
            },
            animation: {
                //duration : 1,
                onComplete : function() {
                    var chartInstance = this.chart,
                    ctx = chartInstance.ctx;

                    ctx.font = Chart.helpers.fontString(
                        Chart.defaults.global.defaultFontSize, 
                        Chart.defaults.global.defaultFontStyle, 
                        Chart.defaults.global.defaultFontFamily
                    );
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';
                    ctx.fillStyle = "#000";

                    this.data.datasets.forEach(function(dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function(bar, index) {
                            var data = dataset.data[index];
                            if (i == 0){
                                ctx.pointStyle = 'triangle';
                                ctx.fillStyle = "#000";
                                // ctx.font = '15px Arial';
                                ctx.fillText(data, bar._model.x - 27, bar._model.y + 8);
                            }
                            else if (i == 1){
                                ctx.fillStyle = "#000";
                                ctx.fillText(data, bar._model.x, bar._model.y + 20);
                                // ctx.font = '12px Arial';
                            }
                            else if (i == 2){
                                ctx.fillStyle = "#000";
                                ctx.fillText(data, bar._model.x, bar._model.y + 15);
                                // ctx.font = '12px Arial';
                            }
                            else if (i == 3){
                                ctx.fillStyle = "#000";
                                ctx.fillText(data, bar._model.x, bar._model.y - 10);
                                // ctx.font = '12px Arial';
                            }
                            else {
                                // ctx.style.backgroundColor = '#FFF';
                                ctx.pointStyle = 'triangle';
                                ctx.fillStyle = "#000";
                                // ctx.font = '15px Arial';
                                ctx.fillText(data, bar._model.x - 27, bar._model.y + 8);
                            }
                            
                        });
                    });
                }
            },
        })
    },
    methods: {
        handle (point, event) {
            const item = event[0]
            // console.log(this.chartData.labels[item._index][0])
            this.$emit('on-receive', {
                index: item._index,
                value: this.chartData.labels[item._index][0]
                // index: 12,
            })
        }
    }
}
