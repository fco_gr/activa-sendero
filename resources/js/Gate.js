export default class Gate{

    constructor(user){
        this.user = user;
    }

    isAdmin(){
        return this.user.type === 'admin';
    }

    isUser(){
        return this.user.type === 'user';
    }

    isCodificador(){
        return this.user.type === 'codificador';
    }
    
    isAdminOrUser(){
        if(this.user.type === 'user' || this.user.type === 'admin'){
            return true;
        }
    }

    isAdminOrCodificador(){
        if(this.user.type === 'codificador' || this.user.type === 'admin'){
            return true;
        }
    }

    isAdminOrAlerta(){
        if(this.user.type === 'admin' || this.user.type === 'alerta'){
            return true;
        }
    }
}
