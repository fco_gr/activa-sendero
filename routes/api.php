<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::resource('user', API\V1\UserController::class)->middleware('auth:api');

Route::middleware('auth:api')->group(function () {

    Route::post('nps', 'API\V1\NpsController@getDatosPorEstudio');
    
    Route::post('drivers', 'API\V1\DriversController@getP1');
    Route::post('drivers_2', 'API\V1\DriversController@getP14');
    
    Route::post('satisfaccion_global', 'API\V1\SatisfaccionGlobalController@getDatosPorEstudio');
    Route::post('satisfaccion_global_evolutivo', 'API\V1\SatisfaccionGlobalController@getDatosEvolutivo');

    Route::post('imagen', 'API\V1\ImagenController@getDatosPorEstudio');
    Route::post('comparativa_parque_p1', 'API\V1\ComparativoParqueController@getDatosP1');
    Route::post('comparativa_parque_p14', 'API\V1\ComparativoParqueController@getDatosP14');
    Route::post('evolutivo_nps', 'API\V1\EvolutivoController@getDatosNps');
    Route::post('evolutivo_p14', 'API\V1\EvolutivoController@getDatosP14');
    
    Route::post('venta', 'API\V1\VentaController@getPreguntas');
    Route::post('venta_atributo/{pregunta}', 'API\V1\VentaController@getAtributos');
    Route::post('acompanamiento', 'API\V1\AcompanamientoController@getPreguntas');
    Route::post('acompanamiento_atributo/{pregunta}', 'API\V1\AcompanamientoController@getAtributos');
    
    Route::post('visita_y_pago', 'API\V1\VisitaYPagoController@getPreguntas');
    Route::post('visita', 'API\V1\VisitaController@getPreguntas');
    Route::post('visita_atributo/{pregunta}', 'API\V1\VisitaController@getAtributos');

    Route::post('pago', 'API\V1\PagoController@getPreguntas');
    Route::post('pago_atributo/{pregunta}', 'API\V1\PagoController@getAtributos');
    
    Route::post('reclamo', 'API\V1\ReclamoController@getPreguntas');
    Route::post('reclamo_atributos/{pregunta}', 'API\V1\ReclamoController@getAtributos');
    
    Route::post('problemas', 'API\V1\ProblemasController@getProblemas');
    Route::post('problemas_nps', 'API\V1\ProblemasController@getP1');
    Route::post('problemas_satis', 'API\V1\ProblemasController@getP14');

    Route::post('informacion_recibida', 'API\V1\InformacionRecibidaController@getData');
    Route::post('resumen_cawi', 'API\V1\ResumenCawiController@getData');



    
    // Route::post('dashboard', 'API\V1\DashboardController@getResumen');
    // Route::post('dashboard_total', 'API\V1\DashboardController@getTotales');
    // Route::post('ranking_local', 'API\V1\RankingLocalController@getRanking');
    // Route::post('benchmark', 'API\V1\BenchmarkController@index');

    // Route::get('viajes/list', 'API\V1\ViajeController@list');
    Route::get('viajes/list', 'API\V1\VariablesFiltroController@listViajes');
    Route::get('familia_productos/list', 'API\V1\FamiliaProductoController@list');
    Route::post('sub_productos/list', 'API\V1\VariablesFiltroController@listSubProductos');
    Route::get('necesidades/list', 'API\V1\VariablesFiltroController@listNecesidades');
    Route::get('parques/list', 'API\V1\VariablesFiltroController@listParques');
    
    Route::get('origen_venta/list', 'API\V1\VariablesFiltroController@listOrigenVenta');
    Route::get('uso_producto/list', 'API\V1\VariablesFiltroController@listUsoPoducto');

    Route::get('pregunta_codificar', 'API\V1\PreguntaCodificarControlle@index');
    Route::get('estudios_codificar', 'API\V1\EstudioController@index');
    Route::get('codificacion', 'API\V1\CodificacionController@index');
    
    Route::get('libro_codigos', 'API\V1\LibroCodigoController@index');
    Route::get('libro_codigos/{id}', 'API\V1\LibroCodigoController@getByPregunta');

    Route::post('respuesta_abierta_codificada_create', 'API\V1\RespuestaAbiertaCodificadaController@create');
    Route::post('respuesta_abierta_codificada_destroy', 'API\V1\RespuestaAbiertaCodificadaController@destroy');

    Route::post('datos_alertas', 'API\V1\DatoConsolidadoAlertaController@index');
    Route::post('mail_alerta', 'API\V1\AlertaController@create');

    Route::get('mediciones', 'API\V1\MedicionesController@getAnnoMes');

    Route::post('descargas', 'API\V1\DescargaController@index');
    Route::post('descargas_meses', 'API\V1\DescargaController@descargaMeses');
    

});



