<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('evolutivo');
});

Route::get('/nps', 'NpsController@index')->name('nps.index');
Route::get('/drivers', 'DriversController')->name('drivers.index');
Route::get('/satisfaccion_global', 'SatisfaccionGlobalController@index')->name('satisfaccion-global.index');
Route::get('/imagen', 'ImagenController@index')->name('imagen.index');
Route::get('/comparativo_parques', 'ComparativoParqueController@index')->name('comparativo-parques.index');
Route::get('/evolutivo', 'EvolutivoController@index')->name('evolutivo.index');

Route::get('/venta', 'VentaController')->name('venta.index');
Route::get('/acompanamiento', 'AcompanamientoController')->name('acompanamiento.index');
Route::get('/visita_y_pago', 'VisitaYPagoController')->name('visita_y_pago.index');
Route::get('/visita', 'VisitaController')->name('visita.index');
Route::get('/pago', 'PagoController')->name('pago.index');
Route::get('/reclamo', 'ReclamoController')->name('reclamo.index');

Route::get('/alertas', 'AlertaController')->name('alertas.index');


Route::get('/problema', 'ProblemasController')->name('problema.index');
Route::get('/informacion_recibida', 'InformacionRecibidaController')->name('informacion_recibida.index');


Route::get('/resumen_cawi', 'ResumenCawiController')->name('resumen_cawi.index');

Route::get('traspaso', 'TraspasoController@index')->name('traspaso');
Route::get('pivot', 'PivotController@index')->name('pivot');

Route::get('traspaso_backup', 'TraspasoBackupController@index')->name('traspaso_backup');

Route::get('/traspaso_abiertas', 'TraspasoPreguntasAbiertasController@traspaso');

Route::get('users', 'UserController')->name('user.index');

Route::get('codificacion', 'CodificacionController')->name('codificacion.index');
Route::get('libro_codigos', 'LibroCodigoController')->name('libro_codigo.index');

Route::get('base_datos', 'BaseDatosController')->name('base_datos.index');


Auth::routes(['register' => false, 'reset' => false]);




