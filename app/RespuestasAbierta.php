<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestasAbierta extends Model
{
    protected $fillable = [
        'estudio_id', 'dato_consolidado_id', 'estudio', 'registro', 'lime_id', 'token', 
        'col_1', 'col_2', 'col_3', 'col_4', 'col_5', 'col_6', 'col_7', 
        'col_8', 'col_9', 'col_10', 'col_11', 'status_col_1', 'status_col_2', 'status_col_3', 
        'status_col_4', 'status_col_5', 'status_col_6', 'status_col_7', 
        'status_col_8', 'status_col_9', 'status_col_10', 'status_col_11',
        'codificado_col_1', 'codificado_col_2', 'codificado_col_3', 
        'codificado_col_4', 'codificado_col_5', 'codificado_col_6', 'codificado_col_7', 
        'codificado_col_8', 'codificado_col_9', 'codificado_col_10', 'codificado_col_11'
    ];

    public function dato_consolidado(){
        return $this->belongsTo(DatoConsolidado::class);
    }

    public function estudio(){
        return $this->belongsTo(Estudio::class);
    }

    public function respuesta_abierta_codificadas(){
        return $this->hasMany(RespuestasAbiertaCodificada::class)->orderBy('libro_codigo_id');
    }


}
