<?php

namespace App\Http\Controllers;

use App\Pregunta;
use App\DatoConsolidado;
use Illuminate\Http\Request;
use App\DatoConsolidadoDetalle;

class PivotController extends Controller
{


    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preguntas = Pregunta::where('detalle', true)->get();

        $datoConsolidados = DatoConsolidado::WhereDoesntHave('DatoConsolidadoDetalles')->limit(1000)->get();

        foreach ($datoConsolidados as $datoConsolidado){
            $datoConsolidadoID = $datoConsolidado->id;
            // dd($datoConsolidadoID);
            foreach ($preguntas as $pregunta){
                //dd($pregunta->tipo);
                
                $preg = $pregunta->pregunta;
                $respuesta = $datoConsolidado->$preg;
                if ($pregunta->tipo == "multiple"){
                    if ($respuesta != '' && $respuesta != null){
                        $respuesta = 1;
                    }
                }
                

                if ($respuesta != '' && $respuesta != '-' && $respuesta != null){
                    if ($respuesta != '8' || ($respuesta == 8 && $preg == 'P1')){
                        $agrega = DatoConsolidadoDetalle::create([
                            'dato_consolidado_id' => $datoConsolidadoID,
                            'pregunta' => $preg,
                            'respuesta' => $respuesta
                        ]);
                    }
                }
            }
        }

        dd('Fin');


    }
}
