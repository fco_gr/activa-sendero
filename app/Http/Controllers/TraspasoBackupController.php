<?php

namespace App\Http\Controllers;

use App\Estudio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TraspasoBackupController extends Controller
{
    public function index(){
        $estudios = Estudio::where('sincroniza', true)->get();
        // dd($estudios);

        foreach ($estudios as $estudio){
            $tipoEstudio = $estudio->tipo_estudio_id;
            $estudioId = $estudio->id;
            $tipoEstudioNombre = $estudio->nom_estudio;
            $codEstudio = $estudio->cod_estudio;

            if ($tipoEstudio == 1){
                $DBdatos = DB::connection('mysql_connect_cati')->table($estudio->tabla)->whereIn('estado', [1, 5])->get();
            }
            //Cawi
            else if ($tipoEstudio == 2){
                if ($tipoEstudioNombre == 'Visita'){
                    $DBdatos = DB::connection('mysql_connect_cawi')->table($estudio->tabla_survey)
                        ->where('Attribute_59', 'Visita')
                        ->join($estudio->tabla_token  . ' as token' , $estudio->tabla_survey .'.token', '=',  'token.token')
                        ->select('id', $estudio->tabla_survey.'.token', 'tid')
                        ->whereNotNull('submitdate')->get();
                    }
                else if ($tipoEstudioNombre == 'Pago'){
                    $DBdatos = DB::connection('mysql_connect_cawi')->table($estudio->tabla_survey)
                        ->where('Attribute_59', 'Pago')
                        ->join($estudio->tabla_token  . ' as token' , $estudio->tabla_survey .'.token', '=',  'token.token')
                        ->select('id', $estudio->tabla_survey.'.token', 'tid')
                        ->whereNotNull('submitdate')->get();
                }
                else {
                    $DBdatos = DB::connection('mysql_connect_cawi')->table($estudio->tabla_survey)
                        ->join($estudio->tabla_token  . ' as token' , $estudio->tabla_survey .'.token', '=',  'token.token')
                        ->select('id', $estudio->tabla_survey.'.token', 'tid')
                        ->whereNotNull('submitdate')->get();
                }
            }

            foreach ($DBdatos as $DBdato){
                // dd($DBdato);
                if ($tipoEstudio == 1){
                    $cod_estudio = $DBdato->ESTUDIO;
                    $registro = $DBdato->REGISTRO;

                    // 

                    if ($tipoEstudioNombre == 'Ventas'){
                        $survey = DB::Table('datos_ventas')
                            ->where('estudio', $cod_estudio)
                            ->where('registro', $registro)
                            ->first();
                        
                        if (!$survey){
                            $new_survey = DB::connection('mysql_connect_cati')->table($estudio->tabla)
                                ->where('estudio', $cod_estudio)
                                ->where('registro', $registro)
                                ->first();

                            DB::table('datos_ventas')->insert((array)$new_survey);
                            
                        }
                    }
                    elseif ($tipoEstudioNombre == 'Acompañamiento'){
                        $survey = DB::Table('datos_acompanamientos')
                            ->where('estudio', $cod_estudio)
                            ->where('registro', $registro)
                            ->first();
                        
                        if (!$survey){
                            $new_survey = DB::connection('mysql_connect_cati')->table($estudio->tabla)
                                ->where('estudio', $cod_estudio)
                                ->where('registro', $registro)
                                ->first();

                            DB::table('datos_acompanamientos')->insert((array)$new_survey);
                            
                        }
                    }

                    // dd($estudio, $registro);
                }
                //Cawi
                else if ($tipoEstudio == 2){
                    $survey = DB::table($estudio->tabla_survey)
                        ->where('id', $DBdato->id)
                        ->where('token', $DBdato->token)
                        ->first();

                    $token = DB::table($estudio->tabla_token)
                        ->where('tid', $DBdato->tid)
                        ->where('token', $DBdato->token)
                        ->first();

                    if(!$survey){
                        $new_survey = DB::connection('mysql_connect_cawi')->table($estudio->tabla_survey)
                            ->where('id', $DBdato->id)
                            ->where('token', $DBdato->token)
                            ->first();
                        DB::table($estudio->tabla_survey)->insert((array)$new_survey);
                    }

                    if(!$token){
                        $new_token = DB::connection('mysql_connect_cawi')->table($estudio->tabla_token)
                            ->where('tid', $DBdato->tid)
                            ->where('token', $DBdato->token)
                            ->first();
                        DB::table($estudio->tabla_token)->insert((array)$new_token);
                    }
                }

            }
        }

        echo "Fin backup";
    }
}
