<?php

namespace App\Http\Controllers;

use App\DatoConsolidadoAbierta;
use DB;
use Illuminate\Http\Request;

class CopiaPreguntasCodificadasController extends Controller
{

    private function copiaAbiertas($idPregunta, $pregunta){

        $sql = "select ";
        $sql .= "null as id, dato_consolidados.estudio_id, '$pregunta' as pregunta, dato_consolidado_id, ";
        $sql .= "dato_consolidados.registro, dato_consolidados.token, ";
        $sql .= "lime_id, libro_codigo_id as codigo, now() as created_at, null as updated_at ";
        $sql .= "from ";
        $sql .= "dato_consolidados, respuestas_abiertas, respuestas_abierta_codificadas ";
        $sql .= "where ";
        $sql .= "dato_consolidados.id = respuestas_abiertas.dato_consolidado_id and ";
        $sql .= "respuestas_abiertas.id = respuestas_abierta_codificadas.respuestas_abierta_id and ";
        $sql .= "dato_consolidado_id in ( ";
        $sql .= "    select id from dato_consolidados where id not in ( ";
        $sql .= "        select distinct dato_consolidado_id from dato_consolidado_abiertas where pregunta = '$pregunta' ";
        $sql .= "   ) ";
        $sql .= ") and pregunta_codificar_id=$idPregunta";


        $datos = DB::select($sql);

        //Inserta datos a la table dato_consolidado_abiertas
        foreach ($datos as $dato) {



            $dato_consolidado_abierta = new DatoConsolidadoAbierta();
            $dato_consolidado_abierta->estudio_id = $dato->estudio_id;
            $dato_consolidado_abierta->pregunta = $dato->pregunta;
            $dato_consolidado_abierta->dato_consolidado_id = $dato->dato_consolidado_id;
            $dato_consolidado_abierta->registro = $dato->registro;
            $dato_consolidado_abierta->token = $dato->token;
            $dato_consolidado_abierta->lime_id = $dato->lime_id;
            $dato_consolidado_abierta->codigo = $dato->codigo;

            try {
                $dato_consolidado_abierta->save();
            }
            catch (\Exception $e) {
                echo $e->getMessage();
            }

        }

    }


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->copiaAbiertas(1, 'P1_1');
        $this->copiaAbiertas(10, 'P14_1');
        $this->copiaAbiertas(11, 'P16');

        echo "Datos copiados";
    }
}
