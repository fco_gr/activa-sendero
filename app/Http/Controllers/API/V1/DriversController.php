<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DriversController extends BaseController
{
    public function getP1(Request $request){
        $sWhere = aplicaFiltros($request);
        // $sWhere = '';
        $decimales = 2;

        $sqlPromotor = "select dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n, count(*) as n, round((count(*) / totales.n) * 100, $decimales) as porc ";
        $sqlPromotor .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros,  ";
        $sqlPromotor .= "( ";
        $sqlPromotor .= "select count(*) as n  ";
        $sqlPromotor .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros ";
        $sqlPromotor .= "where  ";
        $sqlPromotor .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlPromotor .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlPromotor .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlPromotor .= "dato_consolidado_abiertas.pregunta='P1_1' and P1 in (9, 10)               $sWhere         ";
        $sqlPromotor .= ") as totales ";
        $sqlPromotor .= "where  ";
        $sqlPromotor .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlPromotor .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlPromotor .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlPromotor .= "dato_consolidado_abiertas.pregunta='P1_1' and P1 in (9, 10)               $sWhere         ";
        $sqlPromotor .= "group by dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n ";
        $sqlPromotor .= "order by n desc";

        // dd($sqlPromotor);

        $promotores = DB::select($sqlPromotor);
        // dd($promotores);

        $porc = [];
        $labels = [];

        foreach ($promotores as $promotor){
            $labels[] = $promotor->descripcion . " n=" . $promotor->n;
            $porc[] = $promotor->porc;
            $colores[] = "#00b050";
        }

        $aPromotores = [
            'labels' => $labels,
            'porc' => $porc,
            'colores' => $colores,
        ];

        $sqlDetractor = "select dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n, count(*) as n, round((count(*) / totales.n) * 100, $decimales) as porc   ";
        $sqlDetractor .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros,  ";
        $sqlDetractor .= "( ";
        $sqlDetractor .= "select count(*) as n  ";
        $sqlDetractor .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros ";
        $sqlDetractor .= "where  ";
        $sqlDetractor .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlDetractor .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlDetractor .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlDetractor .= "dato_consolidado_abiertas.pregunta='P1_1' and P1 in (0, 1, 2, 3, 4, 5, 6)                  $sWhere      ";
        $sqlDetractor .= ") as totales ";
        $sqlDetractor .= "where  ";
        $sqlDetractor .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlDetractor .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlDetractor .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlDetractor .= "dato_consolidado_abiertas.pregunta='P1_1' and P1 in (0, 1, 2, 3, 4, 5, 6)                   $sWhere   ";
        $sqlDetractor .= "group by dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n ";
        $sqlDetractor .= "order by n desc";


        $detractores = DB::select($sqlDetractor);

        $porc = [];
        $labels = [];
        $colores = [];

        foreach ($detractores as $detractor){
            $labels[] = $detractor->descripcion . " n=" . $detractor->n;
            $porc[] = $detractor->porc;
            $colores[] = "#c00000";
        }

        $aDetractores = [
            'labels' => $labels,
            'porc' => $porc,
            'colores' => $colores,
        ];








        $sqlNeutro = "select dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n, count(*) as n, round((count(*) / totales.n) * 100, $decimales) as porc   ";
        $sqlNeutro .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros,  ";
        $sqlNeutro .= "( ";
        $sqlNeutro .= "select count(*) as n  ";
        $sqlNeutro .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros ";
        $sqlNeutro .= "where  ";
        $sqlNeutro .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlNeutro .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlNeutro .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlNeutro .= "dato_consolidado_abiertas.pregunta='P1_1' and P1 in (7, 8)                  $sWhere      ";
        $sqlNeutro .= ") as totales ";
        $sqlNeutro .= "where  ";
        $sqlNeutro .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlNeutro .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlNeutro .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlNeutro .= "dato_consolidado_abiertas.pregunta='P1_1' and P1 in (7, 8)                   $sWhere   ";
        $sqlNeutro .= "group by dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n ";
        $sqlNeutro .= "order by n desc";


        $neutros = DB::select($sqlNeutro);

        $porc = [];
        $labels = [];
        $colores = [];

        foreach ($neutros as $neutro){
            $labels[] = $neutro->descripcion . " n=" . $neutro->n;
            $porc[] = $neutro->porc;
            $colores[] = "#D1D1CF";
        }

        $aNeutros = [
            'labels' => $labels,
            'porc' => $porc,
            'colores' => $colores,
        ];



        return $this->sendResponse(
            [
                'promotor' => $aPromotores,
                'detractor' => $aDetractores,
                'neutro' => $aNeutros,
            ], 'Datos p1');



    }


    public function getP14(Request $request){
        $sWhere = aplicaFiltros($request);
        // $sWhere = '';
        $decimales = 2;

        $sqlPromotor = "select dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n, count(*) as n, round((count(*) / totales.n) * 100, $decimales) as porc ";
        $sqlPromotor .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros,  ";
        $sqlPromotor .= "( ";
        $sqlPromotor .= "select count(*) as n  ";
        $sqlPromotor .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros ";
        $sqlPromotor .= "where  ";
        $sqlPromotor .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlPromotor .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlPromotor .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlPromotor .= "dato_consolidado_abiertas.pregunta='P14_1' and P14 in (6, 7)               $sWhere         ";
        $sqlPromotor .= ") as totales ";
        $sqlPromotor .= "where  ";
        $sqlPromotor .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlPromotor .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlPromotor .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlPromotor .= "dato_consolidado_abiertas.pregunta='P14_1' and P14 in (6, 7)               $sWhere       ";
        $sqlPromotor .= "group by dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n ";
        $sqlPromotor .= "order by n desc";

        // dd($sqlPromotor);

        $promotores = DB::select($sqlPromotor);
        // dd($promotores);

        $porc = [];
        $labels = [];

        foreach ($promotores as $promotor){
            $labels[] = $promotor->descripcion . " n=" . $promotor->n;
            $porc[] = $promotor->porc;
            $colores[] = "#00b050";
        }

        $aPromotores = [
            'labels' => $labels,
            'porc' => $porc,
            'colores' => $colores,
        ];

        $sqlDetractor = "select dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n, count(*) as n, round((count(*) / totales.n) * 100, 3) as porc   ";
        $sqlDetractor .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros,  ";
        $sqlDetractor .= "( ";
        $sqlDetractor .= "select count(*) as n  ";
        $sqlDetractor .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros ";
        $sqlDetractor .= "where  ";
        $sqlDetractor .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlDetractor .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlDetractor .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlDetractor .= "dato_consolidado_abiertas.pregunta='P14_1' and P14 in (1, 2, 3, 4)               $sWhere         ";
        $sqlDetractor .= ") as totales ";
        $sqlDetractor .= "where  ";
        $sqlDetractor .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlDetractor .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlDetractor .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlDetractor .= "dato_consolidado_abiertas.pregunta='P14_1' and P14 in (1, 2, 3, 4)                $sWhere      ";
        $sqlDetractor .= "group by dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n ";
        $sqlDetractor .= "order by n desc";


        $detractores = DB::select($sqlDetractor);

        $porc = [];
        $labels = [];
        $colores = [];

        foreach ($detractores as $detractor){
            $labels[] = $detractor->descripcion . " n=" . $detractor->n;
            $porc[] = $detractor->porc;
            $colores[] = "#c00000";
        }

        $aDetractores = [
            'labels' => $labels,
            'porc' => $porc,
            'colores' => $colores,
        ];



        





        $sqlNeutro = "select dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n, count(*) as n, round((count(*) / totales.n) * 100, 3) as porc   ";
        $sqlNeutro .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros,  ";
        $sqlNeutro .= "( ";
        $sqlNeutro .= "select count(*) as n  ";
        $sqlNeutro .= "from dato_consolidados, dato_consolidado_abiertas, pregunta_libros ";
        $sqlNeutro .= "where  ";
        $sqlNeutro .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlNeutro .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlNeutro .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlNeutro .= "dato_consolidado_abiertas.pregunta='P14_1' and P14 in (5)               $sWhere         ";
        $sqlNeutro .= ") as totales ";
        $sqlNeutro .= "where  ";
        $sqlNeutro .= "dato_consolidados.id = dato_consolidado_abiertas.dato_consolidado_id and ";
        $sqlNeutro .= "dato_consolidado_abiertas.pregunta = pregunta_libros.pregunta and ";
        $sqlNeutro .= "dato_consolidado_abiertas.codigo = pregunta_libros.codigo and ";
        $sqlNeutro .= "dato_consolidado_abiertas.pregunta='P14_1' and P14 in (5)                $sWhere      ";
        $sqlNeutro .= "group by dato_consolidado_abiertas.codigo, pregunta_libros.descripcion, totales.n ";
        $sqlNeutro .= "order by n desc";


        $neutros = DB::select($sqlNeutro);

        $porc = [];
        $labels = [];
        $colores = [];

        foreach ($neutros as $neutro){
            $labels[] = $neutro->descripcion . " n=" . $neutro->n;
            $porc[] = $neutro->porc;
            $colores[] = "#D1D1CF";
        }

        $aNeutros = [
            'labels' => $labels,
            'porc' => $porc,
            'colores' => $colores,
        ];














        return $this->sendResponse(
            [
                'promotor' => $aPromotores,
                'detractor' => $aDetractores,
                'neutro' => $aNeutros,
            ], 'Datos p1');



    }
}
