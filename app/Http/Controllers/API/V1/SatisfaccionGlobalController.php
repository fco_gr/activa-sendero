<?php

namespace App\Http\Controllers\API\V1;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SatisfaccionGlobalController extends BaseController
{
    public function getDatosPorEstudio(Request $request){

        $sWhere = aplicaFiltros($request);

        $decimales = 1;

        $sql = "";
        $sql .= "select 'Total Sendero' as nom_estudio, 0 AS orden, ";
        $sql .= "sum(if (p14>=1 and p14<=4, 1, 0)) as detractor, sum(if (p14=5, 1, 0)) as neutro, sum(if (p14>=6 and p14<=7, 1, 0)) as promotor, count(*) as n,  ";
        $sql .= "round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p14=5, 1, 0)) / count(*))*100, $decimales) as p_neutro, ";
        $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) as p_promotor,  ";
        $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as neto  ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id  $sWhere  ";
        
        $sql .= " union ";

        // $sql .= "select nom_estudio, min(orden) as orden, ";
        // $sql .= "sum(if (p14>=0 and p14<=6, 1, 0)) as detractor, sum(if (p14 = 5, 1, 0)) as neutro, sum(if (p14>=6 and p14<=7, 1, 0)) as promotor, count(*) as n, ";
        // $sql .= "round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, ";
        // $sql .= "round((sum(if (p14=4, 1, 0)) / count(*))*100, $decimales) as p_neutro, ";
        // $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) as p_promotor, ";
        // $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        // $sql .= "from dato_consolidados, estudios  ";
        // $sql .= "where dato_consolidados.estudio_id = estudios.id  $sWhere   ";
        // $sql .= "group by nom_estudio order by orden";

        $sql .= "select tipo_estudio.nom_estudio, tipo_estudio.orden, detractor, neutro, promotor, n, p_detractor, p_neutro, p_promotor, neto  ";
        $sql .= "from ( ";
        $sql .= "SELECT t.* FROM (SELECT distinct dato_consolidados.tipo_estudio as nom_estudio, estudios.orden from dato_consolidados, estudios where dato_consolidados.estudio_id = estudios.id) t, (SELECT @rownum := 0) r ";
        $sql .= ") as tipo_estudio ";
        $sql .= "LEFT JOIN  ";
        $sql .= "( ";
        $sql .= "select nom_estudio, min(orden) as orden,  ";
        $sql .= "sum(if (p14>=0 and p14<=6, 1, 0)) as detractor, sum(if (p14 = 5, 1, 0)) as neutro, sum(if (p14>=6 and p14<=7, 1, 0)) as promotor, count(*) as n,  ";
        $sql .= "round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as p_detractor,  ";
        $sql .= "round((sum(if (p14=4, 1, 0)) / count(*))*100, $decimales) as p_neutro,  ";
        $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) as p_promotor,  ";
        $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as neto  ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id                         $sWhere    ";
        $sql .= "group by nom_estudio ";
        $sql .= ") as resultados  ";
        $sql .= "ON tipo_estudio.nom_estudio = resultados.nom_estudio  ";
        $sql .= "order by orden;";

        

        // dd($sql);
        
        $estudios = DB::select($sql);

        $neto = [];
        $neutro = [];
        $promotor = [];
        $detractor = [];
        $labels = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->neto;
            $neutro[] = $estudio->p_neutro;
            $promotor[] =  $estudio->p_promotor;
            $detractor[] =  $estudio->p_detractor * -1;
            $labels[] = explode("#", $estudio->nom_estudio . "#n=" . $estudio->n);
        }

        return $this->sendResponse(
            [
                'neto' => $neto,
                'neutro' => $neutro,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos nps');
         
    }


    public function getDatosEvolutivo(Request $request){
        // No es P14es P1
        $campo = $request->campo;
        // dd($campo);
        $whereTipoEstudio = '';
        if ($campo == 1) {
            $whereTipoEstudio .= " and tipo_estudio = 'Ventas' ";
        }
        else if ($campo == 2) {
            $whereTipoEstudio .= " and tipo_estudio = 'Acompañamiento' ";
        }
        else if ($campo == 3) {
            $whereTipoEstudio .= " and tipo_estudio = 'Visita' ";
        }
        else if ($campo == 4) {
            $whereTipoEstudio .= " and tipo_estudio = 'Pago' ";
        }
        else if ($campo == 5) {
            $whereTipoEstudio .= " and tipo_estudio = 'Reclamos' ";
        }
        // dd($whereTipoEstudio);

        $sWhere = aplicaFiltros($request);

        // dd($sWhere);

        $decimales = 1;

        $sql = "";
        $sql .= "select t.agno, t.mes, count(fecha_medicion) as n, ";
        $sql .= "sum(if (P14 >= 1 and P14 <= 4, 1, 0)) as desacuerto,  ";
        $sql .= "sum(if (P14 >= 5 and P14 <= 5, 1, 0)) as neutro, ";
        $sql .= "sum(if (P14 >= 6 and P14 <= 7, 1, 0)) as acuerto, ";
        $sql .= "count(fecha_medicion) as base, ";
        $sql .= "round(sum(if (P14 >= 1 and P14 <= 4, 1, 0)) / count(*) * 100, $decimales) as p_desacuerdo, ";
        $sql .= "round(sum(if (P14 >= 5 and P14 <= 5, 1, 0)) / count(*) * 100, $decimales) as p_neutro, ";
        $sql .= "round(sum(if (P14 >= 6 and P14 <= 7, 1, 0)) / count(*) * 100, $decimales) as p_acuerdo, ";
        $sql .= "round(sum(if (P14 >= 6 and P14 <= 7, 1, 0)) / count(*) * 100, $decimales) - round(sum(if (P14 >= 1 and P14 <= 4, 1, 0)) / count(*) * 100, $decimales) as p_neto ";
        $sql .= "from (select * from (select distinct tipo_estudio from dato_consolidados where 1=1  $sWhere $whereTipoEstudio  ) as estudio, (select distinct year (fecha_medicion) as agno, MONTH (fecha_medicion) as mes from dato_consolidados where 1=1  ) as fechas) as t ";
        $sql .= "left join (select * from dato_consolidados where 1=1 $sWhere $whereTipoEstudio ) as dato_consolidados on t.tipo_estudio = dato_consolidados.tipo_estudio and t.agno = year (fecha_medicion) and t.mes = MONTH (fecha_medicion) ";
        // $sql .= "where 1=1 $sWhere ";
        $sql .= "group by t.agno, t.mes ";
        $sql .= "order by t.agno, t.mes";

        // dd($sql);
        $estudios = DB::select($sql);

        $neto = [];
        $neutro = [];
        $promotor = [];
        $detractor = [];
        $labels = [];

        $meses = ['', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->base > 0 ? $estudio->p_neto : null;
            $neutro[] = $estudio->p_neutro;
            $promotor[] =  $estudio->p_acuerdo;
            $detractor[] =  $estudio->p_desacuerdo * -1;
            // $labels[] = $meses[$estudio->mes] . " " . $estudio->agno; 
            $labels[] = explode("#", $meses[$estudio->mes] . " " . $estudio->agno . "#n=" . $estudio->n);
        }

        // dd($estudios);

        return $this->sendResponse(
            [
                'neto' => $neto,
                'neutro' => $neutro,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ] , 'Datos Evolutivo p14');
         
    }
}
