<?php

namespace App\Http\Controllers\API\V1;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EvolutivoController extends BaseController
{
    public function getDatosNps(Request $request){

        $sWhere = aplicaFiltros($request);

        $decimales = 1;

        $sql = "";
        $sql .= "select t.agno, t.mes, ";
        $sql .= "sum(if (P1 >= 0 and P1 <= 6, 1, 0)) as detractores,  ";
        $sql .= "sum(if (P1 >= 7 and P1 <= 8, 1, 0)) as neutro,  ";
        $sql .= "sum(if (P1 >= 9 and P1 <=10, 1, 0)) as acuerto, count(tipo_estudio) as base, ";
        $sql .= "round(sum(if (P1 >= 0 and P1 <= 6, 1, 0)) / count(*) * 100, 1) as p_desacuerdo, ";
        $sql .= "round(sum(if (P1 >= 7 and P1 <= 8, 1, 0)) / count(*) * 100, 1) as p_neutro, ";
        $sql .= "round(sum(if (P1 >= 9 and P1 <= 10, 1, 0)) / count(*) * 100, 1) as p_acuerdo, ";
        $sql .= "round(sum(if (P1 >= 9 and P1 <= 10, 1, 0)) / count(*) * 100, 1) - round(sum(if (P1 >= 0 and P1 <= 6, 1, 0)) / count(*) * 100, 1) as p_neto ";
        $sql .= "from (select distinct year (fecha_medicion) as agno, MONTH (fecha_medicion) as mes from dato_consolidados) as t left join (select * from dato_consolidados where 1=1 $sWhere ) as v on  ";
        $sql .= "t.agno = year (fecha_medicion) and t.mes = MONTH (fecha_medicion) ";
        $sql .= "group by t.agno, t.mes ";
        $sql .= "order by t.agno, t.mes ";

//        dd($sql);
        $estudios = DB::select($sql);

        $neto = [];
        $neutro = [];
        $promotor = [];
        $detractor = [];
        $labels = [];

        $meses = ['', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->base > 0 ? $estudio->p_neto : null;
            $neutro[] = $estudio->base > 0 ? $estudio->p_neutro : null;
            $promotor[] =  $estudio->p_acuerdo;
            $detractor[] =  $estudio->p_desacuerdo * -1;
            $labels[] = explode("#", $meses[$estudio->mes] . " " . $estudio->agno . "#n=" . $estudio->base); 
        }

        // dd($estudios);

        return $this->sendResponse(
            [
                'neto' => $neto,
                'neutro' => $neutro,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos Evolutivo nps');
         
    }


    public function getDatosP14(Request $request){
        // No es P14es P1

        $sWhere = aplicaFiltros($request);

        // dd($sWhere);

        $decimales = 1;

        $sql = "";
        $sql .= "select t.tipo_estudio, t.agno, t.mes, count(fecha_medicion) as n, ";
        $sql .= "sum(if (P1 >= 0 and P1 <= 6, 1, 0)) as desacuerto,  ";
        $sql .= "sum(if (P1 >= 7 and P1 <= 8, 1, 0)) as neutro, ";
        $sql .= "sum(if (P1 >= 9 and P1 <= 10, 1, 0)) as acuerto, ";
        $sql .= "count(fecha_medicion) as base, ";
        $sql .= "round(sum(if (P1 >= 0 and P1 <= 6, 1, 0)) / count(*) * 100, $decimales) as p_desacuerdo, ";
        $sql .= "round(sum(if (P1 >= 7 and P1 <= 8, 1, 0)) / count(*) * 100, $decimales) as p_acuerdo, ";
        $sql .= "round(sum(if (P1 >= 9 and P1 <= 10, 1, 0)) / count(*) * 100, $decimales) - round(sum(if (P1 >= 0 and P1 <= 6, 1, 0)) / count(*) * 100, $decimales) as p_neto ";
        $sql .= "from (select * from (select distinct tipo_estudio from dato_consolidados where 1=1  $sWhere ) as estudio, (select distinct year (fecha_medicion) as agno, MONTH (fecha_medicion) as mes from dato_consolidados where 1=1  ) as fechas) as t ";
        $sql .= "left join (select * from dato_consolidados where 1=1 $sWhere ) as dato_consolidados on t.tipo_estudio = dato_consolidados.tipo_estudio and t.agno = year (fecha_medicion) and t.mes = MONTH (fecha_medicion) ";
        // $sql .= "where 1=1 $sWhere ";
        $sql .= "group by t.tipo_estudio, t.agno, t.mes ";
        $sql .= "order by t.tipo_estudio, t.agno, t.mes";

        // dd($sql);
        $estudios = DB::select($sql);

        $neto = [];
        $promotor = [];
        $detractor = [];
        $label = [];

        $meses = ['', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

        foreach ($estudios as $estudio){
            $neto[substr($estudio->tipo_estudio, 0, 3)][] = $estudio->base > 0 ? $estudio->p_neto : null;
            $promotor[substr($estudio->tipo_estudio, 0, 3)][] =  $estudio->p_acuerdo;
            $detractor[substr($estudio->tipo_estudio, 0, 3)][] =  $estudio->p_desacuerdo * -1;
            $labels[substr($estudio->tipo_estudio, 0, 3)][] = $meses[$estudio->mes] . " " . $estudio->agno; 
        }

        // dd($estudios);

        return $this->sendResponse(
            [
                'neto' => $neto,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ] , 'Datos Evolutivo p14');
         
    }

    

}
