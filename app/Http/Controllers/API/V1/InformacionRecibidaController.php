<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class InformacionRecibidaController extends BaseController
{
    public function getData(Request $request){

        $sWhere = aplicaFiltros($request);

        $decimal = 1;

        $sql = "select  ";
        $sql .= "sum(if(respuesta = 1, 1, 0)) as resp_si,  ";
        $sql .= "sum(if(respuesta = 2, 1, 0)) as resp_no,  ";
        $sql .= "count(*) as n, ";
        $sql .= "round((sum(if(respuesta = 1, 1, 0)) / count(*) * 100), $decimal) as porc_si, ";
        $sql .= "round((sum(if(respuesta = 2, 1, 0)) / count(*) * 100), $decimal) as porc_no ";
        $sql .= "from dato_consolidados, dato_consolidado_detalles  ";
        $sql .= "where  ";
        $sql .= "dato_consolidados.id = dato_consolidado_detalles.dato_consolidado_id and ";
        $sql .= "pregunta='INFO_P9'             $sWhere ";

        // dd($sql);

        $p9_datos = DB::select($sql); 
        $p9 = [];

        foreach($p9_datos as $p9_dato){
            $p9['data'] = [(double)$p9_dato->porc_si, (double)$p9_dato->porc_no];
            $p9['base'] = $p9_dato->n;
        }

        $sql = "select preguntas.pregunta, preguntas.literal_corto, count(*) as n, (select count(*) as n from dato_consolidado_detalles where pregunta like 'INFO_P10_%') as total, ";
        $sql .= "round(count(*) / (select count(*) as n from dato_consolidado_detalles where pregunta like 'INFO_P10_%') * 100, $decimal) as porc ";
        $sql .= "from dato_consolidados, dato_consolidado_detalles, preguntas  ";
        $sql .= "where  ";
        $sql .= "dato_consolidados.id = dato_consolidado_detalles.dato_consolidado_id and ";
        $sql .= "dato_consolidado_detalles.pregunta = preguntas.pregunta and ";
        $sql .= "dato_consolidado_detalles.pregunta like 'INFO_P10_%'                      $sWhere   ";
        $sql .= "group by preguntas.pregunta, preguntas.literal_corto ";
        $sql .= "order by porc desc";

        $p10_datos = DB::select($sql); 
        $p10 = [];

        foreach($p10_datos as $p10_dato){
            $p10['labels'][] =  explode("#", $p10_dato->literal_corto);
            $p10['porc'][] = $p10_dato->porc;
            $p10['colores'][] = '#c00000';
        }

        $sql =  "select preguntas.pregunta, preguntas.literal_corto,  ";
        $sql .= "sum(if(respuesta >= 1 and respuesta <= 4, 1, 0)) as insat, ";
        $sql .= "sum(if(respuesta >= 5 and respuesta <= 5, 1, 0)) as neutro, ";
        $sql .= "sum(if(respuesta >= 6 and respuesta <= 7, 1, 0)) as sat, ";
        $sql .= "sum(if(respuesta >= 1 and respuesta <= 7, 1, 0)) as base, ";
        $sql .= "round(sum(if(respuesta >= 1 and respuesta <= 4, 1, 0)) / sum(if(respuesta >= 1 and respuesta <= 7, 1, 0)) * 100, $decimal) as porc_insat, ";
        $sql .= "round(sum(if(respuesta >= 5 and respuesta <= 5, 1, 0)) / sum(if(respuesta >= 1 and respuesta <= 7, 1, 0)) * 100, $decimal) as porc_neutro, ";
        $sql .= "round(sum(if(respuesta >= 6 and respuesta <= 7, 1, 0)) / sum(if(respuesta >= 1 and respuesta <= 7, 1, 0)) * 100, $decimal) as porc_sat, ";
        $sql .= "round(sum(if(respuesta >= 6 and respuesta <= 7, 1, 0)) / sum(if(respuesta >= 1 and respuesta <= 7, 1, 0)) * 100, $decimal) - round(sum(if(respuesta >= 1 and respuesta <= 4, 1, 0)) / sum(if(respuesta >= 1 and respuesta <= 7, 1, 0)) * 100, $decimal) as porc_neto ";
        $sql .= "from dato_consolidados, dato_consolidado_detalles, preguntas  ";
        $sql .= "where  ";
        $sql .= "dato_consolidados.id = dato_consolidado_detalles.dato_consolidado_id and ";
        $sql .= "dato_consolidado_detalles.pregunta = preguntas.pregunta and ";
        $sql .= "dato_consolidado_detalles.pregunta like 'INFO_P11_%'                  $sWhere  ";
        $sql .= "group by preguntas.pregunta, preguntas.literal_corto ";
        $sql .= "order by preguntas.pregunta";


        $p10_datos = DB::select($sql); 

        $neto = [];
        $satisfecho = [];
        $insatisfecho = [];
        $neutro = [];
        $labels = [];

        foreach ($p10_datos as $p10_dato){
            $neto[] = $p10_dato->porc_neto;
            $satisfecho[] =  $p10_dato->porc_sat;
            $neutro[] =  $p10_dato->porc_neutro;
            $insatisfecho[] =  $p10_dato->porc_insat * -1;
            $labels[] = explode("#", $p10_dato->literal_corto . "#n=" . $p10_dato->base); 
        }


        return $this->sendResponse(
            [
                'p9' => $p9,
                'p10' => $p10,
                'p11' => [
                    'satisfecho' => $satisfecho,
                    'insatisfecho' => $insatisfecho,
                    'neutro' => $neutro,
                    'neto' => $neto,
                    'labels' => $labels,
                ]
            ], 
            'Datos información recibida'
        );


    }
}
