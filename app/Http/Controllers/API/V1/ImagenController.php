<?php

namespace App\Http\Controllers\API\V1;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ImagenController extends BaseController
{
    public function getDatosPorEstudio(Request $request){

        $sWhere = aplicaFiltros($request);

        $decimales = 1;

        $sql = "";
        $sql .= "select dato_consolidado_detalles.pregunta, preguntas.literal_corto, sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) as desacuerto,  ";
        $sql .= "sum(if (respuesta = 5, 1, 0)) as neutro, sum(if (respuesta >= 6 and respuesta <=7, 1, 0)) as acuerto, count(*) as base, ";
        $sql .= "round(sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) / count(*) * 100, 1) as p_desacuerdo, ";
        $sql .= "round(sum(if (respuesta >= 5 and respuesta <=5, 1, 0)) / count(*) * 100, 1) as p_neutro, ";
        $sql .= "round(sum(if (respuesta >= 6 and respuesta <=7, 1, 0)) / count(*) * 100, 1) as p_acuerdo, ";
        $sql .= "round(sum(if (respuesta >= 6 and respuesta <=7, 1, 0)) / count(*) * 100, 1) - round(sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) / count(*) * 100, $decimales) as p_neto ";
        $sql .= "from dato_consolidados, dato_consolidado_detalles, preguntas  ";
        $sql .= "where  ";
        $sql .= "dato_consolidados.id = dato_consolidado_detalles.dato_consolidado_id and ";
        $sql .= "dato_consolidado_detalles.pregunta = preguntas.pregunta and  ";
        $sql .= "dato_consolidado_detalles.pregunta in ('P2_1', 'P2_2', 'P2_3', 'P2_4', 'P2_5', 'P2_6')           $sWhere   ";
        $sql .= "group by dato_consolidado_detalles.pregunta, preguntas.literal_corto";

        
        $estudios = DB::select($sql);

        $neto = [];
        $neutro = [];
        $promotor = [];
        $detractor = [];
        $labels = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->p_neto;
            $neutro[] = $estudio->p_neutro;
            $promotor[] =  $estudio->p_acuerdo;
            $detractor[] =  $estudio->p_desacuerdo * -1;
            $labels[] = explode("#", $estudio->literal_corto . "#n=" .$estudio->base) ;
        }

        // dd($estudios);

        return $this->sendResponse(
            [
                'neto' => $neto,
                'neutro' => $neutro,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos imagen');
         
    }
}
