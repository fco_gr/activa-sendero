<?php

namespace App\Http\Controllers\API\V1;

use App\Estudio;
use App\DatoConsolidado;
use App\PreguntaCodificar;
use App\RespuestasAbierta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class CodificacionController extends BaseController
{
    public function __construct(){
        $this->middleware('auth:api');
    }

    public function index(Request $request){
        if (!Gate::allows('isAdminOrCodificador')) {
            return $this->unauthorizedResponse();
        }
        // $this->authorize('isAdminOrCodificador');

        $pregunta = \request()->get('pregunta') !== null ? \request()->get('pregunta') : '';
        $estudio = \request()->get('estudio') !== null ? \request()->get('estudio') : '';
        $estado = \request()->get('estado') !== null ? \request()->get('estado') : '';
        // dd($pregunta, $estudio, $estado);



        $datos = RespuestasAbierta::with(['dato_consolidado', 'estudio'])
            ->with(['respuesta_abierta_codificadas' => function($query) use ($pregunta){
                return $query->where('pregunta_codificar_id', $pregunta);
            }])
            ->whereHas('estudio', function($query){
                return $query->where('sincroniza', true);
            })
            // ->whereHas('dato_consolidado', function($query) use ($pregunta){
            //     $col = "col_" . $pregunta;
            //     // dd($col);
            //     return $query->whereNotNull($col);
            // })
            ->where(function($query) use ($pregunta){
                return $query->whereNotNull("col_" . $pregunta);
            })
            ->orderBy('id', 'DESC');

        if ($estudio != ''){
            $estudios = DB::table('estudios')
                ->select(DB::raw("group_concat(estudios.id) as ids"))
                ->where('nom_estudio', $estudio)
                ->first();
            $ids = explode(",", $estudios->ids);
            $datos = $datos->whereIn('estudio_id', $ids);
        }

        if ($estado == 1){
            $datos = $datos->whereDoesntHave('respuesta_abierta_codificadas', function($query) use ($pregunta){
                return $query->where('pregunta_codificar_id', $pregunta);
            });
        }
        else {
            $datos = $datos->whereHas('respuesta_abierta_codificadas', function($query) use ($pregunta){
                return $query->where('pregunta_codificar_id', $pregunta);
            });
        }


        $datos = $datos->paginate(10);
        // dd($datos);

        return $this->sendResponse($datos, 'Preguntas a codificar');
    }
}
