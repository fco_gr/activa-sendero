<?php

namespace App\Http\Controllers\API\V1;

use App\LibroCodigo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class LibroCodigoController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        if (!Gate::allows('isAdminOrCodificador')) {
            return $this->unauthorizedResponse();
        }
        // $this->authorize('isAdminOrCodificador');

        $pregunta = \request()->get('pregunta') !== null ? \request()->get('pregunta') : '';
        $descripcion = \request()->get('descripcion') !== null ? \request()->get('descripcion') : '';
        // dd($pregunta, $descripcion);


        $libro_codigos = LibroCodigo::where('pregunta_codificar_id', $pregunta);
        if ($descripcion != ''){
            $libro_codigos = $libro_codigos->where('descripcion', 'LIKE', "%$descripcion%");
        }
        $libro_codigos = $libro_codigos->get();

        return $this->sendResponse($libro_codigos, 'Preguntas a codificar');
    }

    public function getByPregunta($id){
        $libro_codigos = LibroCodigo::where('pregunta_codificar_id', $id)->get();
        return $this->sendResponse($libro_codigos, 'Libro códigos');
    }
}
