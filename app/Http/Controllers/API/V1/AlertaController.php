<?php

namespace App\Http\Controllers\API\V1;

use App\Alerta;
use App\Mail\AlertaMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AlertaController extends BaseController
{
    public function __construct(){
        $this->middleware('auth:api');
    }

    public function create(Request $request){

        $validated = $request->validate([
            // 'email' => 'required|email',
            "emails"    => "required|array|min:1",
            "emails.*.direccion"  => "required|distinct|string|email",
            'mensaje' => 'required',
        ]);

        $mails = [];
        foreach($request->emails as $email){
            
            array_push($mails, $email["direccion"]);
        }
        $emails_text = implode(", ", $mails);


        try {
            $alerta = Alerta::create([
                'dato_consolidado_id' => $request->dato['id'],
                'user_id' => Auth::user()->id,
                'email' => $emails_text,
                'mensaje' => $request->mensaje,
            ]);
    
            $correo = new AlertaMailable($request->all());
            Mail::to($mails)->send($correo);
            
        } catch (\Throwable $th) {
            //throw $th;
            Log::debug($th->getMessage());
        }

        

        return $this->sendResponse($alerta, 'Correo enviado satisfactoriamente');

        
    }
    
}
