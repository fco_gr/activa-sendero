<?php

namespace App\Http\Controllers\API\V1;

use App\DatoConsolidado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MedicionesController extends BaseController
{
    public function getAnnoMes(){

        $sql = "select distinct year(fecha_medicion) * 100 + month(fecha_medicion) as fecha_numero,  ";
        $sql .= "case ";
        $sql .= "    when month(fecha_medicion) = 1 then concat('Enero', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 2 then concat('Febero', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 3 then concat('Marzo', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 4 then concat('Abril', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 5 then concat('Mayo', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 6 then concat('Junio', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 7 then concat('Julio', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 8 then concat('Agosto', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 9 then concat('Septiembre', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 10 then concat('Octubre', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 11 then concat('Noviembre', ' ', year(fecha_medicion)) ";
        $sql .= "    when month(fecha_medicion) = 12 then concat('Diciembre', ' ', year(fecha_medicion)) ";
        $sql .= "end as fecha_texto ";
        $sql .= "from dato_consolidados ";
        $sql .= "where year(fecha_medicion) in  (2022, 2023, 2024) ";
        $sql .= "order by fecha_numero desc;";

        $mediciones = DB::select($sql);
        // $mediciones = [];
        // foreach ($meses as $mes){
        //     $mediciones[$mes->fecha_medicion_anno * 1000 + $mes->fecha_medicion_mes] = $mes->mes_texto;
        // }

        return $this->sendResponse($mediciones, 'Lista mediciones');
    }
}
