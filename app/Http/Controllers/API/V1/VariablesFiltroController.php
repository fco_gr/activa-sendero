<?php

namespace App\Http\Controllers\API\V1;

use App\DatoConsolidado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VariablesFiltroController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    

    public function listViajes()
    {
        $tipoEstudio = DatoConsolidado::select('tipo_estudio')->orderBy('tipo_estudio')->distinct()->pluck('tipo_estudio');
        return $this->sendResponse($tipoEstudio, 'Lista tipo estudios');
    }


    public function listSubProductos(Request $request)
    {
        // dd($request->familia);

        $lista = [];
        foreach ($request->familia as $fam){
            if($fam == 'Cremaciones'){
                $lista[] = 'CREMACION';
                $lista[] = 'PLAN DE CREMACION';
            }
            else if($fam == 'Sepultura'){
                $lista[] = 'SEPULTURA';
                $lista[] = 'PLAN DE SEPULTURA';
                $lista[] = 'JARDIN';
            }
            else if($fam == 'Columbarios'){
                $lista[] = 'COLUMBARIO';
                $lista[] = 'PLAN DE COLUMBARIO';
            }
        }

        // dd($lista);

        // CREMACION
        // SEPULTURA
        // PLAN DE SEPULTURA
        // PLAN DE CREMACION
        // JARDIN
        
        $tipoSubProductos = DatoConsolidado::select('TIPO_SUB_PRODUCTO')->whereIn('TIPO_SUB_PRODUCTO', $lista)->orderBy('TIPO_SUB_PRODUCTO')->distinct()->pluck('TIPO_SUB_PRODUCTO');
        return $this->sendResponse($tipoSubProductos, 'Lista sub productos');
    }

    public function listNecesidades()
    {
        $tipoNecesidades = DatoConsolidado::select('TIPO_NECES_OP')->orderBy('TIPO_NECES_OP')->distinct()->get()->pluck('TIPO_NECES_OP');
        return $this->sendResponse($tipoNecesidades, 'Lista necesidades');
    }

    
    public function listParques()
    {
        $tipoParques = DatoConsolidado::select('PARQUE_FISICO')->orderBy('PARQUE_FISICO')->distinct()->get()->pluck('PARQUE_FISICO');
        return $this->sendResponse($tipoParques, 'Lista Parques');
    }


    public function listOrigenVenta()
    {
        $tipoOrigenVenta = DatoConsolidado::select('tipo_origen_venta')->orderBy('tipo_origen_venta')->distinct()->get()->pluck('tipo_origen_venta');
        return $this->sendResponse($tipoOrigenVenta, 'Lista Origen Venta');
    }

    public function listUsoPoducto()
    {
        $tipoParques = DatoConsolidado::select('IND_USO_PRODUCTO')->orderBy('IND_USO_PRODUCTO')->distinct()->get()->pluck('IND_USO_PRODUCTO');
        return $this->sendResponse($tipoParques, 'Lista Uso de Producto');
    }


}
