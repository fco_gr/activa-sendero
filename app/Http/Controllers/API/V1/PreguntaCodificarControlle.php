<?php

namespace App\Http\Controllers\API\V1;

use App\PreguntaCodificar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\API\V1\BaseController;

class PreguntaCodificarControlle extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        if (!Gate::allows('isAdminOrCodificador')) {
            return $this->unauthorizedResponse();
        }

        $preguntas = PreguntaCodificar::all();

        
        return $this->sendResponse($preguntas, 'Listado de preguntas a codificar');

    }
}
