<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RespuestasAbiertaCodificada;
use Illuminate\Support\Facades\Auth;

class RespuestaAbiertaCodificadaController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function create(Request $request){
        $request['user_id'] = Auth::user()->id;
        $respuesta_abierta_codificado = RespuestasAbiertaCodificada::create($request->all());
        return $this->sendResponse($respuesta_abierta_codificado, 'Codificado');
    }

    public function destroy(Request $request){
        $respuesta_abierta_codificado = RespuestasAbiertaCodificada::where('respuestas_abierta_id', $request->respuestas_abierta_id)
            ->where('pregunta_codificar_id', $request->pregunta_codificar_id)
            ->where('libro_codigo_id', $request->libro_codigo_id);
            
        $respuesta_abierta_codificado = $respuesta_abierta_codificado->delete();
        return $this->sendResponse($respuesta_abierta_codificado, 'Codificado');
    }
}
