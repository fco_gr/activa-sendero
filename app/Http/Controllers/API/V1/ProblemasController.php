<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProblemasController extends BaseController
{

    public function getProblemas(Request $request){
        $sWhere = aplicaFiltros($request);
        // $sWhere = '';
        $decimales = 0;

        $sql = "select ' Total Sendero' as nom_estudio, 0 AS orden, sum(if (p15=1, 1, 0)) as si, sum(if (p15=2, 1, 0)) as no, count(*) as n, ";
        $sql .= "round(sum(if (p15=1, 1, 0)) / count(*) * 100, $decimales) as porc ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15 is not null                  $sWhere  ";
        $sql .= "union ";
        $sql .= "select nom_estudio, min(orden) as orden, sum(if (p15=1, 1, 0)) as si, sum(if (p15=2, 1, 0)) as no, count(*) as n, ";
        $sql .= "round(sum(if (p15=1, 1, 0)) / count(*) * 100, $decimales) as porc ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15 is not null                 $sWhere   ";
        $sql .= "group by nom_estudio  order by orden;";

        // dd($sqlPromotor);

        $problemas = DB::select($sql);
        // dd($promotores);

        $porc = [];
        $labels = [];

        foreach ($problemas as $problema){
            $labels[] = $problema->nom_estudio . " n=" . $problema->n;
            $porc[] = $problema->porc;
            $colores[] = "#c00000";
        }

        
        return $this->sendResponse(
            [
                'labels' => $labels,
                'porc' => $porc,
                'colores' => $colores,
            ], 'Datos porcentajes de problemas');

    }

    public function getP1(Request $request){
        $sWhere = aplicaFiltros($request);

        $decimales = 1;

        $sql = "";
        $sql .= "select 'Total Sendero' as nom_estudio, 0 AS orden, sum(if (p1>=0 and p1<=6, 1, 0)) as detractor, sum(if (p1>=7 and p1<=8, 1, 0)) as neutro, sum(if (p1>=9 and p1<=10, 1, 0)) as promotor, count(*) as n,  ";
        $sql .= "round((sum(if (p1>=0 and p1<=6,  1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p1>=7 and p1<=8, 1, 0)) / count(*))*100, $decimales) as p_neutro, round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) as p_promotor,  ";
        $sql .= "round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*)) * 100, $decimales) - round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15=1        $sWhere    ";
        
        $sql .= "union ";

        $sql .= "select tipo_estudio as nom_estudio, min(orden) as orden, sum(if (p1>=0 and p1<=6, 1, 0)) as detractor, sum(if (p1>=7 and p1<=8, 1, 0)) as neutro, sum(if (p1>=9 and p1<=10, 1, 0)) as promotor, count(*) as n, ";
        $sql .= "round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p1>=7 and p1<=8, 1, 0)) / count(*))*100, $decimales) as p_neutro, round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) as p_promotor, ";
        $sql .= "round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        $sql .= "from dato_consolidados, estudios  ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15=1         $sWhere   ";
        $sql .= "group by tipo_estudio order by orden";

        // dd($sql);

        $estudios = DB::select($sql);

        $neto = [];
        $promotor = [];
        $detractor = [];
        $labels = [];
        $colores = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->neto;
            $promotor[] =  $estudio->p_promotor;
            $detractor[] =  $estudio->p_detractor * -1;
            // $labels[] = explode("#", $estudio->nom_estudio . "#n=" . $estudio->n); 
            $labels[] = $estudio->nom_estudio; 
            $colores[] = "#c00000";
        }

        $aConProblemas = [
            'neto' => $neto,
            'promotor' => $promotor,
            'detractor' => $detractor,
            'labels' => $labels,
            'colores' => $colores,
        ];



        /* Sin problemas */

        $sql = "";
        $sql .= "select 'Total Sendero' as nom_estudio, 0 AS orden, sum(if (p1>=0 and p1<=6, 1, 0)) as detractor, sum(if (p1>=7 and p1<=8, 1, 0)) as neutro, sum(if (p1>=9 and p1<=10, 1, 0)) as promotor, count(*) as n,  ";
        $sql .= "round((sum(if (p1>=0 and p1<=6,  1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p1>=7 and p1<=8, 1, 0)) / count(*))*100, $decimales) as p_neutro, round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) as p_promotor,  ";
        $sql .= "round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*)) * 100, $decimales) - round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15=2        $sWhere    ";
        
        $sql .= "union ";

        $sql .= "select tipo_estudio as nom_estudio, min(orden) as orden, sum(if (p1>=0 and p1<=6, 1, 0)) as detractor, sum(if (p1>=7 and p1<=8, 1, 0)) as neutro, sum(if (p1>=9 and p1<=10, 1, 0)) as promotor, count(*) as n, ";
        $sql .= "round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p1>=7 and p1<=8, 1, 0)) / count(*))*100, $decimales) as p_neutro, round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) as p_promotor, ";
        $sql .= "round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        $sql .= "from dato_consolidados, estudios  ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15=2         $sWhere   ";
        $sql .= "group by tipo_estudio order by orden";

        // dd($sql);

        $estudios = DB::select($sql);

        $neto = [];
        $promotor = [];
        $detractor = [];
        $labels = [];
        $colores = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->neto;
            $promotor[] =  $estudio->p_promotor;
            $detractor[] =  $estudio->p_detractor * -1;
            // $labels[] = explode("#", $estudio->nom_estudio . "#n=" . $estudio->n); 
            $labels[] = $estudio->nom_estudio;
            $colores[] = "#3daadf";
        }

        $aSinProblemas = [
            'neto' => $neto,
            'promotor' => $promotor,
            'detractor' => $detractor,
            'labels' => $labels,
            'colores' => $colores,
        ];

        return $this->sendResponse(
            [
                'con_problemas' => $aConProblemas,
                'sin_problemas' => $aSinProblemas
            ]
            , 'Datos nps');
    }


    public function getP14(Request $request){
        $sWhere = aplicaFiltros($request);

        $decimales = 1;

        $sql = "";
        $sql .= "select 'Total Sendero' as nom_estudio, 0 AS orden, sum(if (p14>=1 and p14<=4, 1, 0)) as detractor, sum(if (p14=5, 1, 0)) as neutro, sum(if (p14>=6 and p14<=7, 1, 0)) as promotor, count(*) as n,  ";
        $sql .= "round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p14=5, 1, 0)) / count(*))*100, $decimales) as p_neutro, round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) as p_promotor,  ";
        $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as neto  ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15=1   $sWhere  ";
        
        $sql .= " union ";

        $sql .= "select nom_estudio, min(orden) as orden, sum(if (p14>=0 and p14<=6, 1, 0)) as detractor, sum(if (p14 = 5, 1, 0)) as neutro, sum(if (p14>=6 and p14<=7, 1, 0)) as promotor, count(*) as n, ";
        $sql .= "round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p14=4, 1, 0)) / count(*))*100, $decimales) as p_neutro, round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) as p_promotor, ";
        $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        $sql .= "from dato_consolidados, estudios  ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15=1  $sWhere   ";
        $sql .= "group by nom_estudio order by orden";

        $estudios = DB::select($sql);

        $neto = [];
        $promotor = [];
        $detractor = [];
        $labels = [];
        $colores = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->neto;
            $promotor[] =  $estudio->p_promotor;
            $detractor[] =  $estudio->p_detractor * -1;
            // $labels[] = explode("#", $estudio->nom_estudio . "#n=" . $estudio->n); 
            $labels[] = $estudio->nom_estudio;
            $colores[] = "#c00000";
        }

        $aConProblemas = [
            'neto' => $neto,
            'promotor' => $promotor,
            'detractor' => $detractor,
            'labels' => $labels,
            'colores' => $colores,
        ];


        /** Sin Problemas */

        $sql = "";
        $sql .= "select 'Total Sendero' as nom_estudio, 0 AS orden, sum(if (p14>=1 and p14<=4, 1, 0)) as detractor, sum(if (p14=5, 1, 0)) as neutro, sum(if (p14>=6 and p14<=7, 1, 0)) as promotor, count(*) as n,  ";
        $sql .= "round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p14=5, 1, 0)) / count(*))*100, $decimales) as p_neutro, round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) as p_promotor,  ";
        $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as neto  ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15=2   $sWhere  ";
        
        $sql .= " union ";

        $sql .= "select nom_estudio, min(orden) as orden, sum(if (p14>=0 and p14<=6, 1, 0)) as detractor, sum(if (p14 = 5, 1, 0)) as neutro, sum(if (p14>=6 and p14<=7, 1, 0)) as promotor, count(*) as n, ";
        $sql .= "round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, round((sum(if (p14=4, 1, 0)) / count(*))*100, $decimales) as p_neutro, round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) as p_promotor, ";
        $sql .= "round((sum(if (p14>=6 and p14<=7, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p14>=1 and p14<=4, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        $sql .= "from dato_consolidados, estudios  ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id and p15=2  $sWhere   ";
        $sql .= "group by nom_estudio order by orden";

        $estudios = DB::select($sql);

        $neto = [];
        $promotor = [];
        $detractor = [];
        $labels = [];
        $colores = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->neto;
            $promotor[] =  $estudio->p_promotor;
            $detractor[] =  $estudio->p_detractor * -1;
            // $labels[] = explode("#", $estudio->nom_estudio . "#n=" . $estudio->n); 
            $labels[] = $estudio->nom_estudio;
            $colores[] = "#3daadf";
        }

        $aSinProblemas = [
            'neto' => $neto,
            'promotor' => $promotor,
            'detractor' => $detractor,
            'labels' => $labels,
            'colores' => $colores,
        ];


        return $this->sendResponse(
            [
                'con_problemas' => $aConProblemas,
                'sin_problemas' => $aSinProblemas
            ]
            , 'Datos satisfaccion');

        



    }
}
