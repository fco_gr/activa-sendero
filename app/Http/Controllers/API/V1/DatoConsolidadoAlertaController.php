<?php

namespace App\Http\Controllers\API\V1;

use Carbon\Carbon;
use App\DatoConsolidado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class DatoConsolidadoAlertaController extends BaseController
{
    public function __construct(){
        $this->middleware('auth:api');
    }


    public function index(Request $request){

        if (!Gate::allows('isAdminOrAlerta')) {
            return $this->unauthorizedResponse();
        }

        // $sWhere = aplicaFiltros($request);
        // dd($request->all());

        $viajes = $request->filter && $request->filter["viaje_id"] ? $request->filter["viaje_id"] : [];
        $familias = $request->filter && $request->filter["familia_id"] ? $request->filter["familia_id"] : [];
        $tipoProductos = $request->filter && $request->filter["tipo_producto_id"] ? $request->filter["tipo_producto_id"] : [];
        $funeraria = $request->filter && $request->filter["funeraria_id"] ? $request->filter["funeraria_id"] : '';
        $necesidades = $request->filter && $request->filter["necesidad_id"] ? $request->filter["necesidad_id"] : [];
        $parques = $request->filter && $request->filter["parque_id"] ? $request->filter["parque_id"] : [];

        $fecha_ini = $request->filter && $request->filter["fecha_ini"] != null ? Carbon::parse($request->filter["fecha_ini"])->format('Y-m-d') : null;
        $fecha_fin = $request->filter && $request->filter["fecha_fin"] != null ? Carbon::parse($request->filter["fecha_fin"])->format('Y-m-d') : null;
        
        $origen_ventas = $request->filter && $request->filter["origen_venta_id"] ? $request->filter["origen_venta_id"] : [];
        $uso_productos = $request->filter && $request->filter["uso_producto_id"] ? $request->filter["uso_producto_id"] : [];


        $satisfaccions = $request->filter && $request->filter["satisfaccion"] ? $request->filter["satisfaccion"] : [];
        $alerta = $request->filter && $request->filter["alerta"] ? $request->filter["alerta"] : [];

        $datos_consolidados = DatoConsolidado::select('id', 'estudio_id', 'fecha_medicion', 'RUT_CLNT_TIT', 'NMB_CLNT_TIT', 'COD_AREA_FONO_FIJO', 'FONO_FIJO', 'FONO_CELULAR', 'EMAIL_CLIE', 'TIPO_NECES_OP', 'TIPO_PRODUCTO', 'PARQUE_FISICO', 'P14', 'P14_1', 'P15', 'P16', 'P17', 'FRACCION', 'MANZANA')
            ->with(['estudio', 'alertas'])
            ->orderBy('fecha_medicion', 'DESC')
            ->orderBy('estudio_id', 'ASC');

        if (count($viajes) > 0){
            $datos_consolidados = $datos_consolidados->whereHas('estudio', function($query) use ($viajes){
                return $query->whereIn('nom_estudio', $viajes);
            });
        }

        if (count($tipoProductos) > 0){
            // dd($tipoProductos);
            $datos_consolidados = $datos_consolidados->whereIn('TIPO_SUB_PRODUCTO', $tipoProductos);
        }
        else if (count($familias) > 0){
            $textoFiltrar = [];
            if (in_array("Sepultura", $familias)){
                array_push($textoFiltrar, 'SEPULTURA', 'PLAN DE SEPULTURA', 'JARDIN');
            }
            if (in_array("Cremaciones", $familias)){
                array_push($textoFiltrar, 'CREMACION', 'PLAN DE CREMACION');
            }
            if (in_array("Columbarios", $familias)){
                array_push($textoFiltrar, 'COLUMBARIO', 'PLAN DE COLUMBARIO');
            }
            // dd($textoFiltrar);
            $datos_consolidados = $datos_consolidados->whereIn('TIPO_SUB_PRODUCTO', $textoFiltrar);
        }

        if (strlen($funeraria) > 0){
            if ($funeraria == "PARQUE DEL SENDERO"){
                $datos_consolidados = $datos_consolidados->whereIn('FUNERARIA', ['PARQUE DEL SENDERO']);
            }
            else if ($funeraria == "IVAN MARTINEZ"){
                $datos_consolidados = $datos_consolidados->whereIn('FUNERARIA', ['IVAN MARTINEZ (SANTIAGO)', 'IVAN MARTINEZ']);
            }
            else if ($funeraria == "OTRAS"){
                $datos_consolidados = $datos_consolidados->whereNotIn('FUNERARIA', ['PARQUE DEL SENDERO', 'IVAN MARTINEZ (SANTIAGO)', 'IVAN MARTINEZ']);
            }
        }

        
        if (count($necesidades) > 0){
            $datos_consolidados = $datos_consolidados->whereIn('TIPO_NECES_OP', $necesidades);
        }

        if (count($parques) > 0){
            $datos_consolidados = $datos_consolidados->whereIn('PARQUE_FISICO', $parques);
        }

        if ($fecha_ini && $fecha_fin){
            $datos_consolidados = $datos_consolidados->whereBetween('fecha_medicion', [$fecha_ini, $fecha_fin]);
        }

        if (count($origen_ventas) > 0){
            $datos_consolidados = $datos_consolidados->whereIn('tipo_origen_venta', $origen_ventas);
        }

        if (count($uso_productos) > 0){
            $datos_consolidados = $datos_consolidados->whereIn('IND_USO_PRODUCTO', $uso_productos);
        }

        // $origen_ventas
        // $uso_productos



        if ($alerta){
            if ($alerta=='Si'){
                $datos_consolidados = $datos_consolidados->whereHas('alertas');
            }
            elseif ($alerta=='No'){
                $datos_consolidados = $datos_consolidados->doesntHave('alertas');
            }
            
        }

        if (count($satisfaccions) > 0){
            $datos_consolidados = $datos_consolidados->whereIn('P14', $satisfaccions);
        }
    
        $datos_consolidados = $datos_consolidados->paginate(20);
        return $this->sendResponse($datos_consolidados, 'Datos consolidados con alertas');
    }
}
