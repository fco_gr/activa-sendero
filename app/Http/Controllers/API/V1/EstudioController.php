<?php

namespace App\Http\Controllers\API\V1;

use App\Estudio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EstudioController extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $estudios = Estudio::where('sincroniza', true)->get();
        return $this->sendResponse($estudios, 'Listado de estudios');
    }
}
