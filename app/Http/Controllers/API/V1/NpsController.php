<?php

namespace App\Http\Controllers\API\V1;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class NpsController extends BaseController
{
    public function getDatosPorEstudio(Request $request){

        $sWhere = aplicaFiltros($request);

        $decimales = 1;

        $sql = "";
        $sql .= "select ' Total Sendero' as nom_estudio, 0 AS orden, sum(if (p1>=0 and p1<=6, 1, 0)) as detractor, sum(if (p1>=7 and p1<=8, 1, 0)) as neutro, sum(if (p1>=9 and p1<=10, 1, 0)) as promotor, count(*) as n,  ";
        $sql .= "round((sum(if (p1>=0 and p1<=6,  1, 0)) / count(*)) * 100, $decimales) as p_detractor, ";
        $sql .= "round((sum(if (p1>=7 and p1<=8, 1, 0)) / count(*))*100, $decimales) as p_neutro, ";
        $sql .= "round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) as p_promotor,  ";
        $sql .= "round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*)) * 100, $decimales) - round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        $sql .= "from dato_consolidados, estudios   ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id        $sWhere    ";
        
        $sql .= " union ";

        $sql .= "select nom_estudio, min(orden) as orden, sum(if (p1>=0 and p1<=6, 1, 0)) as detractor, sum(if (p1>=7 and p1<=8, 1, 0)) as neutro, sum(if (p1>=9 and p1<=10, 1, 0)) as promotor, count(*) as n, ";
        $sql .= "round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as p_detractor, ";
        $sql .= "round((sum(if (p1>=7 and p1<=8, 1, 0)) / count(*))*100, $decimales) as p_neutro, ";
        $sql .= "round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) as p_promotor, ";
        $sql .= "round((sum(if (p1>=9 and p1<=10, 1, 0)) / count(*))*100, $decimales) - round((sum(if (p1>=0 and p1<=6, 1, 0)) / count(*)) * 100, $decimales) as neto ";
        $sql .= "from dato_consolidados, estudios  ";
        $sql .= "where dato_consolidados.estudio_id = estudios.id         $sWhere   ";
        $sql .= "group by nom_estudio order by orden";

        // dd($sql);
        $estudios = DB::select($sql);

        $neto = [];
        $promotor = [];
        $detractor = [];
        $neutro = [];
        $labels = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->neto;
            $promotor[] =  $estudio->p_promotor;
            $neutro[] =  $estudio->p_neutro;
            // $neutro[] =  $estudio->p_promotor - $estudio->p_detractor;
            $detractor[] =  $estudio->p_detractor * -1;
            $labels[] = explode("#", $estudio->nom_estudio . "#n=" . $estudio->n); 
        }

        // dd($estudios);

        return $this->sendResponse(
            [
                'neto' => $neto,
                'neutro' => $neutro,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos nps2');
         
    }
}
