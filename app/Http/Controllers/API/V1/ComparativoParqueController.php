<?php

namespace App\Http\Controllers\API\V1;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ComparativoParqueController extends BaseController
{
    public function getDatosP1(Request $request){

        $sWhere = aplicaFiltros($request);

        $decimales = 2;

        $sql = "";
        $sql .= "select ' TOTAL SENDERO' as PARQUE_FISICO,  ";
        $sql .= "sum(if (P1 >= 0 and P1 <= 6, 1, 0)) as detractores,  ";
        $sql .= "sum(if (P1 >= 7 and P1 <= 8, 1, 0)) as neutro,  ";
        $sql .= "sum(if (P1 >= 9 and P1 <=10, 1, 0)) as acuerto, count(*) as base, ";
        $sql .= "round(sum(if (P1 >= 0 and P1 <= 6, 1, 0)) / count(*) * 100, $decimales) as p_desacuero, ";
        $sql .= "round(sum(if (P1 >= 7 and P1 <= 8, 1, 0)) / count(*) * 100, $decimales) as p_neutro, ";
        $sql .= "round(sum(if (P1 >= 9 and P1 <= 10, 1, 0)) / count(*) * 100, $decimales) as p_acuerdo, ";
        $sql .= "round(sum(if (P1 >= 9 and P1 <= 10, 1, 0)) / count(*) * 100, $decimales) - round(sum(if (P1 >= 0 and P1 <= 6, 1, 0)) / count(*) * 100, $decimales) as p_neto ";
        $sql .= "from dato_consolidados      where 1=1      $sWhere   ";
        $sql .= "union ";
        $sql .= "select PARQUE_FISICO,  ";
        $sql .= "sum(if (P1 >= 0 and P1 <= 6, 1, 0)) as detractores,  ";
        $sql .= "sum(if (P1 >= 7 and P1 <= 8, 1, 0)) as neutro,  ";
        $sql .= "sum(if (P1 >= 9 and P1 <=10, 1, 0)) as acuerto, count(*) as base, ";
        $sql .= "round(sum(if (P1 >= 0 and P1 <= 6, 1, 0)) / count(*) * 100, $decimales) as p_desacuero, ";
        $sql .= "round(sum(if (P1 >= 7 and P1 <= 8, 1, 0)) / count(*) * 100, $decimales) as p_neutro, ";
        $sql .= "round(sum(if (P1 >= 9 and P1 <= 10, 1, 0)) / count(*) * 100, $decimales) as p_acuerdo, ";
        $sql .= "round(sum(if (P1 >= 9 and P1 <= 10, 1, 0)) / count(*) * 100, $decimales) - round(sum(if (P1 >= 0 and P1 <= 6, 1, 0)) / count(*) * 100, $decimales) as p_neto ";
        $sql .= "from dato_consolidados       where 1=1     $sWhere   ";
        $sql .= "group by PARQUE_FISICO ";
        $sql .= "order by PARQUE_FISICO";

        // dd($sql);
        
        $estudios = DB::select($sql);

        $neto = [];
        $neutro = [];
        $promotor = [];
        $detractor = [];
        $labels = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->p_neto;
            $neutro[] = $estudio->p_neutro;
            $promotor[] =  $estudio->p_acuerdo;
            $detractor[] =  $estudio->p_desacuero * -1;
            $labels[] = explode('#', ucwords(strtolower($estudio->PARQUE_FISICO)) . "#n=" . $estudio->base);
        }

        // dd($estudios);

        return $this->sendResponse(
            [
                'neto' => $neto,
                'neutro' => $neutro,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos comparativo p1');
         
    }

    public function getDatosP14(Request $request){

        $sWhere = aplicaFiltros($request);

        $decimales = 2;

        $sql = "";
        $sql .= "select ' TOTAL SENDERO' as PARQUE_FISICO, sum(if (P14 >= 1 and P14 <=4, 1, 0)) as desacuerto,  ";
        $sql .= "sum(if (P14 = 5, 1, 0)) as neutro, sum(if (P14 >= 6 and P14 <=7, 1, 0)) as acuerto, count(*) as base, ";
        $sql .= "round(sum(if (P14 >= 1 and P14 <=4, 1, 0)) / count(*) * 100, $decimales) as p_desacuero, ";
        $sql .= "round(sum(if (P14 >= 6 and P14 <=7, 1, 0)) / count(*) * 100, $decimales) as p_acuerdo, ";
        $sql .= "round(sum(if (P14 >= 6 and P14 <=7, 1, 0)) / count(*) * 100, $decimales) - round(sum(if (P14 >= 1 and P14 <=4, 1, 0)) / count(*) * 100, $decimales) as p_neto ";
        $sql .= "from dato_consolidados    where 1=1   $sWhere          ";
        $sql .= "union ";
        $sql .= "select PARQUE_FISICO, sum(if (P14 >= 1 and P14 <=4, 1, 0)) as desacuerto,  ";
        $sql .= "sum(if (P14 = 5, 1, 0)) as neutro, sum(if (P14 >= 6 and P14 <=7, 1, 0)) as acuerto, count(*) as base, ";
        $sql .= "round(sum(if (P14 >= 1 and P14 <=4, 1, 0)) / count(*) * 100, $decimales) as p_desacuero, ";
        $sql .= "round(sum(if (P14 >= 6 and P14 <=7, 1, 0)) / count(*) * 100, $decimales) as p_acuerdo, ";
        $sql .= "round(sum(if (P14 >= 6 and P14 <=7, 1, 0)) / count(*) * 100, $decimales) - round(sum(if (P14 >= 1 and P14 <=4, 1, 0)) / count(*) * 100, 1) as p_neto ";
        $sql .= "from dato_consolidados    where 1=1     $sWhere ";
        $sql .= "group by PARQUE_FISICO  ";
        $sql .= "order by PARQUE_FISICO";

        // dd($sql);
        
        $estudios = DB::select($sql);

        $neto = [];
        $promotor = [];
        $detractor = [];
        $label = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->p_neto;
            $promotor[] =  $estudio->p_acuerdo;
            
            $detractor[] =  $estudio->p_desacuero * -1;
            // if ($estudio->p_desacuero == 0){
            //     $estudio->p_desacuero = '-';
            // }
            $labels[] = explode("#", $estudio->PARQUE_FISICO . "#n=" . $estudio->base);
        }

        // dd($estudios);

        return $this->sendResponse(
            [
                'neto' => $neto,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos comparativo p1');
         
    }
}
