<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PagoController extends BaseController
{
    public function getAtributos(Request $request, $pregunta){
        
        $sWhere = aplicaFiltros($request);

        $preguntaLike = substr($pregunta, 0, strrpos($pregunta, '_') + 1);

        $decimales = 2;

        $sql = "";
        $sql .= "select dato_consolidado_detalles.pregunta, preguntas.literal_corto,   ";
        $sql .= "sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) as desacuerto,  ";
        $sql .= "sum(if (respuesta = 5, 1, 0)) as neutro,  ";
        $sql .= "sum(if (respuesta >= 6 and respuesta <=7, 1, 0)) as acuerto,  ";
        $sql .= "count(*) as base, ";
        $sql .= "round(sum(if (respuesta >= 1 and respuesta <= 4, 1, 0)) / count(*) * 100, 2) as p_desacuerdo, ";
        $sql .= "round(sum(if (respuesta >= 5 and respuesta <= 5, 1, 0)) / count(*) * 100, 2) as p_neutro, ";
        $sql .= "round(sum(if (respuesta >= 6 and respuesta <= 7, 1, 0)) / count(*) * 100, 2) as p_acuerdo, ";
        $sql .= "round(sum(if (respuesta >= 6 and respuesta <= 7, 1, 0)) / count(*) * 100, 2) - round(sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) / count(*) * 100, $decimales) as p_neto ";
        $sql .= "from dato_consolidados, dato_consolidado_detalles, preguntas  ";
        $sql .= "where  ";
        $sql .= "dato_consolidados.id = dato_consolidado_detalles.dato_consolidado_id and  ";
        $sql .= "dato_consolidado_detalles.pregunta = preguntas.pregunta and tipo_estudio = 'Pago' and ";
        $sql .= "dato_consolidado_detalles.pregunta like '$preguntaLike%' and  dato_consolidado_detalles.pregunta <> '$pregunta'      $sWhere    ";
        $sql .= "group by dato_consolidado_detalles.pregunta, preguntas.literal_corto ";
        $sql .= "order by dato_consolidado_detalles.pregunta ";

        // dd($sql);
        
        $estudios = DB::select($sql);

        $neto = [];
        $neutro = [];
        $promotor = [];
        $detractor = [];
        $labels = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->p_neto;
            $neutro[] = $estudio->p_neutro;
            $promotor[] =  $estudio->p_acuerdo;
            $detractor[] =  $estudio->p_desacuerdo * -1;
            
            // $campo = str_replace("VENTA_", "", $estudio->pregunta) . "#" . $estudio->literal_corto;
            $campo = $estudio->pregunta . '#' . $estudio->literal_corto;
            $labels[] = explode("#", $campo . "#n=" .$estudio->base) ;
        }

        return $this->sendResponse(
            [
                'neto' => $neto,
                'neutro' => $neutro,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos venta');
         
    }

    public function getPreguntas(Request $request){
        
        $sWhere = aplicaFiltros($request);

        $decimales = 2;

        $sql = "select * from  ";
        $sql .= "( ";
        $sql .= "select * from  ";
        $sql .= "( ";
        $sql .= "select (@row_number:=@row_number + 1) AS num, preguntas.id as p_id, preguntas.pregunta AS p_pregunta, preguntas.literal_corto as lit_corto from preguntas, (SELECT @row_number:=0) AS t where pregunta in ('PAGO_P8_6', 'PAGO_P11_4', 'P14') ";
        $sql .= ") as preg ";
        $sql .= ") as preguntas left join  ";
        $sql .= "( ";
        $sql .= "select preguntas.id, preguntas.pregunta, preguntas.literal_corto,    ";
        $sql .= "sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) as desacuerto,   ";
        $sql .= "sum(if (respuesta = 5, 1, 0)) as neutro,   ";
        $sql .= "sum(if (respuesta >= 6 and respuesta <=7, 1, 0)) as acuerto,   ";
        $sql .= "count(*) as base, ";
        $sql .= "round(sum(if (respuesta >= 1 and respuesta <= 4, 1, 0)) / count(*) * 100, 2) as p_desacuerdo,  ";
        $sql .= "round(sum(if (respuesta >= 5 and respuesta <= 5, 1, 0)) / count(*) * 100, 2) as p_neutro,  ";
        $sql .= "round(sum(if (respuesta >= 6 and respuesta <= 7, 1, 0)) / count(*) * 100, 2) as p_acuerdo,  ";
        $sql .= "round(sum(if (respuesta >= 6 and respuesta <= 7, 1, 0)) / count(*) * 100, 2) - round(sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) / count(*) * 100, 2) as p_neto  ";
        $sql .= "from dato_consolidados, dato_consolidado_detalles, preguntas   ";
        $sql .= "where   ";
        $sql .= "dato_consolidados.id = dato_consolidado_detalles.dato_consolidado_id and   ";
        $sql .= "dato_consolidado_detalles.pregunta = preguntas.pregunta and  ";
        $sql .= "dato_consolidado_detalles.pregunta in ('PAGO_P8_6', 'PAGO_P11_4', 'P14') and tipo_estudio = 'Pago'  ";
        $sql .= "                              $sWhere      ";
        $sql .= "group by preguntas.id,  preguntas.pregunta, preguntas.literal_corto ";
        $sql .= ") as datos on p_pregunta=datos.pregunta order by p_id";

        
        $estudios = DB::select($sql);

        $neto = [];
        $neutro = [];
        $promotor = [];
        $detractor = [];
        $labels = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->p_neto;
            $neutro[] = $estudio->p_neutro;
            $promotor[] =  $estudio->p_acuerdo;
            $detractor[] =  $estudio->p_desacuerdo * -1;
            $campo = str_replace("PAGO_", "", $estudio->pregunta) . "#" . $estudio->literal_corto;
            $labels[] = explode("#", $campo . "#n=" .$estudio->base) ;
        }

        return $this->sendResponse(
            [
                'neto' => $neto,
                'neutro' => $neutro,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos venta');
         
    }
}
