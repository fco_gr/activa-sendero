<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class ResumenCawiController extends BaseController
{
    public function getData(Request $request){

        if (!Gate::allows('isAdmin')) {
            return $this->unauthorizedResponse();
        }

        $mes = $request['mes'];

        $sql = "select nom_estudio, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NI' and TIPO_PRODUCTO='SEPULTURA', 1, 0)) as NI_SEP, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NI' and TIPO_PRODUCTO='CREMACIÓN', 1, 0)) as NI_CRE, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NI', 1, 0)) as NI_TOT, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NF' and TIPO_PRODUCTO='SEPULTURA', 1, 0)) as NF_SEP, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NF' and TIPO_PRODUCTO='CREMACIÓN', 1, 0)) as NF_CRE, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NF', 1, 0)) as NF_TOT, ";
        $sql .= "COUNT(*) as TOTAL ";
        $sql .= "from dato_consolidados, estudios  ";
        $sql .= "where  ";
        $sql .= "dato_consolidados.estudio_id = estudios.id and  ";
        $sql .= "nom_estudio in ('Visita', 'Pago', 'Reclamos') and  ";
        $sql .= "month(fecha_medicion)=$mes  ";
        $sql .= "group by nom_estudio ";
        $sql .= "order by nom_estudio";

        $datos = DB::select($sql);

        $sql = "select ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NI' and TIPO_PRODUCTO='SEPULTURA', 1, 0)) as NI_SEP, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NI' and TIPO_PRODUCTO='CREMACIÓN', 1, 0)) as NI_CRE, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NI', 1, 0)) as NI_TOT, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NF' and TIPO_PRODUCTO='SEPULTURA', 1, 0)) as NF_SEP, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NF' and TIPO_PRODUCTO='CREMACIÓN', 1, 0)) as NF_CRE, ";
        $sql .= "SUM(if (TIPO_NECES_OP = 'Cliente NF', 1, 0)) as NF_TOT, ";
        $sql .= "COUNT(*) as TOTAL ";
        $sql .= "from dato_consolidados, estudios  ";
        $sql .= "where  ";
        $sql .= "dato_consolidados.estudio_id = estudios.id and  ";
        $sql .= "nom_estudio in ('Visita', 'Pago', 'Reclamos') and  ";
        $sql .= "month(fecha_medicion)=$mes  ";

        $datos_total = DB::select($sql);

        return $this->sendResponse(
            [
                'datos' => $datos, 
                'total' => $datos_total[0], 
            ],
            'Datos resumen cawi');
    }
}
