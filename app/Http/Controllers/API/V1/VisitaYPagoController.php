<?php

namespace App\Http\Controllers\API\V1;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class VisitaYPagoController extends BaseController
{
    public function getPreguntas(Request $request){
        
        $sWhere = aplicaFiltros($request);

        $decimales = 2;

        $sql = "";
        $sql .= "select dato_consolidado_detalles.pregunta, preguntas.literal_corto,   ";
        $sql .= "sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) as desacuerto,  ";
        $sql .= "sum(if (respuesta = 5, 1, 0)) as neutro,  ";
        $sql .= "sum(if (respuesta >= 6 and respuesta <=7, 1, 0)) as acuerto,  ";
        $sql .= "count(*) as base, ";
        $sql .= "round(sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) / count(*) * 100, 2) as p_desacuerdo, ";
        $sql .= "round(sum(if (respuesta >= 6 and respuesta <=7, 1, 0)) / count(*) * 100, 2) as p_acuerdo, ";
        $sql .= "round(sum(if (respuesta >= 6 and respuesta <=7, 1, 0)) / count(*) * 100, 2) - round(sum(if (respuesta >= 1 and respuesta <=4, 1, 0)) / count(*) * 100, $decimales) as p_neto ";
        $sql .= "from dato_consolidados, dato_consolidado_detalles, preguntas  ";
        $sql .= "where  ";
        $sql .= "dato_consolidados.id = dato_consolidado_detalles.dato_consolidado_id and  ";
        $sql .= "dato_consolidado_detalles.pregunta = preguntas.pregunta and ";
        $sql .= "dato_consolidado_detalles.pregunta in ('PAGO_P6_8', 'PAGO_P8_6', 'PAGO_P11_4', 'P14')        $sWhere    ";
        $sql .= "group by dato_consolidado_detalles.pregunta, preguntas.literal_corto";

        
        $estudios = DB::select($sql);

        $neto = [];
        $acuerdo = [];
        $desacuerdo = [];
        $labels = [];

        foreach ($estudios as $estudio){
            $neto[] = $estudio->p_neto;
            $promotor[] =  $estudio->p_acuerdo;
            $detractor[] =  $estudio->p_desacuerdo * -1;
            $labels[] = explode("#", $estudio->literal_corto . "#n=" .$estudio->base) ;
        }

        // dd($estudios);

        return $this->sendResponse(
            [
                'neto' => $neto,
                'promotor' => $promotor,
                'detractor' => $detractor,
                'labels' => $labels,
            ]
            , 'Datos venta');
         
    }
}
