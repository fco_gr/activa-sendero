<?php

namespace App\Http\Controllers\API\V1;

use App\FamiliaProducto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FamiliaProductoController extends BaseController
{
    protected $familia_producto = '';

    public function __construct(FamiliaProducto $familia_producto)
    {
        $this->middleware('auth');
        $this->familia_producto = $familia_producto;
    }

    public function list()
    {
        // dd('por acá');
        $familia_productos = $this->familia_producto->pluck('descripcion');
        
        return $this->sendResponse($familia_productos, 'Users list');
    }
}
