<?php

namespace App\Http\Controllers\API\V1;

use App\Viaje;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ViajeController extends BaseController
{
    protected $viaje = '';

    public function __construct(Viaje $viaje)
    {
        $this->middleware('auth');
        $this->viaje = $viaje;
    }

    public function list()
    {
        $viajes = $this->viaje->pluck('descripcion');
        
        return $this->sendResponse($viajes, 'Lista de viajes');
    }
}
