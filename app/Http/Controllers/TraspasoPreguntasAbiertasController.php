<?php

namespace App\Http\Controllers;

use App\Estudio;
use App\DatoConsolidado;
use App\RespuestasAbierta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TraspasoPreguntasAbiertasController extends Controller
{
    public function traspaso(){
        $estudios = Estudio::where('sincroniza', true)->orderBy('id')->get();
        // dd($estudios);

        foreach ($estudios as $estudio){
            if ($estudio->tipo_estudio_id == 1){
                $estudio_id = $estudio->id;
                $table = $estudio->tabla;
                if ($estudio->nom_estudio == 'Ventas'){
                    $datoConsolidados = DatoConsolidado::where('estudio_id', $estudio_id)
                        ->doesntHave('respuestas_abiertas')
                        ->get();

                    foreach ($datoConsolidados as $datoConsolidado){
                        $dato_consolidado_id = $datoConsolidado->id;
                        $estudio = $datoConsolidado->estudio;
                        $registro = $datoConsolidado->registro;

                        $sql = "select estudio, registro, P1_1_TXT as col_1, P6_TXT as col_3, ";
                        $sql .= "P7_TXT as col_4, P9_1_TXT as col_7, P10_1_TXT as col_10, P12 as col_11 ";
                        $sql .= "from datos_ventas where estudio like '$estudio%' and registro=$registro";

                        $respuesta = DB::select($sql);
                        
                        if ($respuesta){
                            $respuesta = $respuesta[0];
                            $respuestaAbierta = RespuestasAbierta::create([
                                'estudio_id' => $estudio_id,
                                'dato_consolidado_id' => $dato_consolidado_id,
                                'estudio' => $estudio,
                                'registro' => $registro,
                                'col_1' => $respuesta->col_1 != '-' ? $respuesta->col_1 : null,
                                'col_3' => $respuesta->col_3 != '-' ? $respuesta->col_3 : null,
                                'col_4' => $respuesta->col_4 != '-' ? $respuesta->col_4 : null,
                                'col_7' => $respuesta->col_7 != '-' ? $respuesta->col_7 : null,
                                'col_10' => $respuesta->col_10 != '-' ? $respuesta->col_10 : null,
                                'col_11' => $respuesta->col_11 != '-' ? $respuesta->col_11 : null,
    
                                'status_col_1' => $respuesta->col_1 != '-',
                                'status_col_3' => $respuesta->col_3 != '-',
                                'status_col_4' => $respuesta->col_4 != '-',
                                'status_col_7' => $respuesta->col_7 != '-',
                                'status_col_10' => $respuesta->col_10 != '-',
                                'status_col_11' => $respuesta->col_11 != '-',
                            ]);
                        }

                    }
                }

                else if ($estudio->nom_estudio == 'Acompañamiento'){
                    $datoConsolidados = DatoConsolidado::where('estudio_id', $estudio_id)
                        ->doesntHave('respuestas_abiertas')
                        ->get();

                    // dd($datoConsolidados);

                    foreach ($datoConsolidados as $datoConsolidado){
                        $dato_consolidado_id = $datoConsolidado->id;
                        $estudio = $datoConsolidado->estudio;
                        $registro = $datoConsolidado->registro;

                        $sql = "select estudio, registro, P1_1_TXT as col_1, P7_TXT as col_5, P14_1_TXT as col_10, P16 as col_11 ";
                        $sql .= "from datos_acompanamientos where estudio like '$estudio%' and registro=$registro";

                        $respuesta = DB::select($sql);
                        
                        if ($respuesta){
                            $respuesta = $respuesta[0];
                            // dd($respuesta);
                            $respuestaAbierta = RespuestasAbierta::create([
                                'estudio_id' => $estudio_id,
                                'dato_consolidado_id' => $dato_consolidado_id,
                                'estudio' => $estudio,
                                'registro' => $registro,
                                'col_1' => $respuesta->col_1 != '-' ? $respuesta->col_1 : null,
                                'col_5' => $respuesta->col_5 != '-' ? $respuesta->col_5 : null,
                                'col_10' => $respuesta->col_10 != '-' ? $respuesta->col_10 : null,
                                'col_11' => $respuesta->col_11 != '-' ? $respuesta->col_11 : null,
    
                                'status_col_1' => $respuesta->col_1 != '-',
                                'status_col_5' => $respuesta->col_5 != '-',
                                'status_col_10' => $respuesta->col_10 != '-',
                                'status_col_11' => $respuesta->col_11 != '-',
                            ]);
                        }
                    }
                }

            }
            else {
                $estudio_id = $estudio->id;
                $tabla = $estudio->tabla_survey;

                if ($estudio->nom_estudio == 'Visita'){
                    $datoConsolidados = DatoConsolidado::where('estudio_id', $estudio_id)
                        ->doesntHave('respuestas_abiertas')
                        ->get();

                    foreach ($datoConsolidados as $datoConsolidado){
                        $dato_consolidado_id = $datoConsolidado->id;
                        $token = $datoConsolidado->token;

                        // dd($datoConsolidado);
                        $sql = "select id as lime_id, token, 220356X4587X97235 as col_1, 220356X4591X97240other as col_6, ";
                        $sql .= "220356X4592X97243other as col_8, 220356X4592X97245 as col_9, 220356X4593X97247 as col_10, "; 
                        $sql .= "220356X4594X97249 as col_11 ";
                        $sql .= "from $tabla where token='$token'";

                        $respuesta = DB::select($sql);
                        
                        if ($respuesta){
                            $respuesta = $respuesta[0];
                            // dd($respuesta);
                            $respuestaAbierta = RespuestasAbierta::create([
                                'estudio_id' => $estudio_id,
                                'dato_consolidado_id' => $dato_consolidado_id,
                                'lime_id' => $respuesta->lime_id,
                                'token' => $token,
                                'col_1' => $respuesta->col_1 ? $respuesta->col_1 : null,
                                'col_6' => $respuesta->col_6 ? $respuesta->col_6 : null,
                                'col_8' => $respuesta->col_8 ? $respuesta->col_8 : null,
                                'col_9' => $respuesta->col_9 ? $respuesta->col_9 : null,
                                'col_10' => $respuesta->col_10 ? $respuesta->col_10 : null,
                                'col_11' => $respuesta->col_11 ? $respuesta->col_11 : null,
    
                                'status_col_1' => $respuesta->col_1 ? true : false,
                                'status_col_6' => $respuesta->col_6 ? true : false,
                                'status_col_8' => $respuesta->col_8 ? true : false,
                                'status_col_9' => $respuesta->col_9 ? true : false,
                                'status_col_10' => $respuesta->col_10 ? true : false,
                                'status_col_11' => $respuesta->col_11 ? true : false,
                            ]);
                        }
                    }
                }
                else if ($estudio->nom_estudio == 'Pago'){
                    $datoConsolidados = DatoConsolidado::where('estudio_id', $estudio_id)
                        ->doesntHave('respuestas_abiertas')
                        ->get();

                    // dd($datoConsolidados);

                    foreach ($datoConsolidados as $datoConsolidado){
                        $dato_consolidado_id = $datoConsolidado->id;
                        $token = $datoConsolidado->token;

                        // dd($datoConsolidado);
                        $sql = "select id as lime_id, token, 220356X4587X97235 as col_1, 220356X4591X97240other as col_6, ";
                        $sql .= "220356X4592X97243other as col_8, 220356X4592X97245 as col_9, 220356X4593X97247 as col_10, "; 
                        $sql .= "220356X4594X97249 as col_11 ";
                        $sql .= "from $tabla where token='$token'";

                        $respuesta = DB::select($sql);
                        
                        if ($respuesta){
                            $respuesta = $respuesta[0];
                            // dd($respuesta);
                            $respuestaAbierta = RespuestasAbierta::create([
                                'estudio_id' => $estudio_id,
                                'dato_consolidado_id' => $dato_consolidado_id,
                                'lime_id' => $respuesta->lime_id,
                                'token' => $token,
                                'col_1' => $respuesta->col_1 ? $respuesta->col_1 : null,
                                'col_6' => $respuesta->col_6 ? $respuesta->col_6 : null,
                                'col_8' => $respuesta->col_8 ? $respuesta->col_8 : null,
                                'col_9' => $respuesta->col_9 ? $respuesta->col_9 : null,
                                'col_10' => $respuesta->col_10 ? $respuesta->col_10 : null,
                                'col_11' => $respuesta->col_11 ? $respuesta->col_11 : null,
    
                                'status_col_1' => $respuesta->col_1 ? true : false,
                                'status_col_6' => $respuesta->col_6 ? true : false,
                                'status_col_8' => $respuesta->col_8 ? true : false,
                                'status_col_9' => $respuesta->col_9 ? true : false,
                                'status_col_10' => $respuesta->col_10 ? true : false,
                                'status_col_11' => $respuesta->col_11 ? true : false,
                            ]);
                        }
                    }
                }
                else if ($estudio->nom_estudio == 'Reclamos'){
                    $datoConsolidados = DatoConsolidado::where('estudio_id', $estudio_id)
                        ->doesntHave('respuestas_abiertas')
                        ->get();

                    // dd($datoConsolidados);

                    foreach ($datoConsolidados as $datoConsolidado){
                        $dato_consolidado_id = $datoConsolidado->id;
                        $token = $datoConsolidado->token;

                        // dd($datoConsolidado);
                        $sql = "select id as lime_id, token, 220332X4579X97190 as col_1, 220332X4582X97194other as col_2, ";
                        $sql .= "220332X4585X97199other as col_8, 220332X4585X97201 as col_9, 220332X4586X97203 as col_10 ";
                        $sql .= "from $tabla where token='$token'";

                        $respuesta = DB::select($sql);
                        
                        if ($respuesta){
                            $respuesta = $respuesta[0];
                            // dd($respuesta);
                            $respuestaAbierta = RespuestasAbierta::create([
                                'estudio_id' => $estudio_id,
                                'dato_consolidado_id' => $dato_consolidado_id,
                                'lime_id' => $respuesta->lime_id,
                                'token' => $token,
                                'col_1' => $respuesta->col_1 ? $respuesta->col_1 : null,
                                'col_2' => $respuesta->col_2 ? $respuesta->col_2 : null,
                                'col_8' => $respuesta->col_8 ? $respuesta->col_8 : null,
                                'col_9' => $respuesta->col_9 ? $respuesta->col_9 : null,
                                'col_10' => $respuesta->col_10 ? $respuesta->col_10 : null,
    
                                'status_col_1' => $respuesta->col_1 ? true : false,
                                'status_col_2' => $respuesta->col_2 ? true : false,
                                'status_col_8' => $respuesta->col_8 ? true : false,
                                'status_col_9' => $respuesta->col_9 ? true : false,
                                'status_col_10' => $respuesta->col_10 ? true : false,
                            ]);
                        }
                    }
                }
                
            }
        }

        echo "Fin traspaso abiertas";
    }
}
