<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResumenCawiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke(Request $request)
    {
        return view('resumen_cawi.index');
    }
}
