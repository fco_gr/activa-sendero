<?php

namespace App\Http\Controllers;

use App\Estudio;
use App\DatoConsolidado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DiccionarioEstudioPregunta;

class TraspasoController extends Controller
{

    private function DiccionarioCampos($estudioId){
        $diccionarios = DiccionarioEstudioPregunta::where('estudio_id', $estudioId)->get();
        $campos = [];
        foreach ($diccionarios as $diccionario){
            $campos[$diccionario->origen] = $diccionario->destino;
        }
        return ($campos);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudios = Estudio::where('sincroniza', true)->get();
        // dd($estudios);

        foreach ($estudios as $estudio){
            // dd($estudio);
            $tipoEstudio = $estudio->tipo_estudio_id;
            $estudioId = $estudio->id;
            $tipoEstudioNombre = $estudio->nom_estudio;
            $codEstudio = $estudio->cod_estudio;
            // dd($codEstudio);

            // dd($tipoEstudioNombre);

            $diccionarioPreguntas = $this->DiccionarioCampos($estudioId);
            // $camposComa = DB::select('SELECT GROUP_CONCAT(origen) as origen FROM diccionario_estudio_preguntas where estudio_id = ' . $estudioId);

            // $camposComa = DiccionarioEstudioPregunta::select(DB::raw('GROUP_CONCAT(origen) as origen'))->where('estudio_id', $estudioId)->first();
            $camposComa = DiccionarioEstudioPregunta::select('origen')->where('estudio_id', $estudioId)->get()->toArray();
            $camposComaArray = [];

            foreach ($camposComa as $camp){
                // dd($camp);
                $camposComaArray[] = $camp['origen'];
            }

            // dd($tipoEstudio);
            // dd($camposComaArray);

            $DBdatos = null;
            $arrayInsert = [];
            $nombreToken = '';
            // Cati
            if ($tipoEstudio == 1){
                $DBdatos = DB::connection('mysql_connect_cati')->table($estudio->tabla)->select($camposComaArray)->whereIn('estado', [1, 5])->get();

            }
            //Cawi
            else if ($tipoEstudio == 2){
                if ($tipoEstudioNombre == 'Visita'){
                    $DBdatos = DB::connection('mysql_connect_cawi')->table($estudio->tabla_survey)
                        ->where('Attribute_59', 'Visita')
                        ->join($estudio->tabla_token  . ' as token' , $estudio->tabla_survey .'.token', '=',  'token.token')
                        ->select($camposComaArray)->whereNotNull('submitdate')->get();
                    }
                else if ($tipoEstudioNombre == 'Pago'){
                    $DBdatos = DB::connection('mysql_connect_cawi')->table($estudio->tabla_survey)
                        ->where('Attribute_59', 'Pago')
                        ->join($estudio->tabla_token  . ' as token' , $estudio->tabla_survey .'.token', '=',  'token.token')
                        ->select($camposComaArray)->whereNotNull('submitdate')->get();
                }
                else {
                    $DBdatos = DB::connection('mysql_connect_cawi')->table($estudio->tabla_survey)
                        ->join($estudio->tabla_token  . ' as token' , $estudio->tabla_survey .'.token', '=',  'token.token')
                        ->select($camposComaArray)->whereNotNull('submitdate')->get();
                }
            }

            foreach ($DBdatos as $DBdato){
                // $arrayInsert['id'] = null;
                $arrayInsert['estudio_id'] = $estudioId;
                if ($tipoEstudio == 2 && ($tipoEstudioNombre == 'Visita' || $tipoEstudioNombre == 'Pago')){
                    $arrayInsert['tipo_estudio'] = ucwords(strtolower($DBdato->Attribute_59));
                }else {
                    $arrayInsert['tipo_estudio'] = $tipoEstudioNombre;
                }

                if ($tipoEstudio == 2 && ($tipoEstudioNombre == 'Visita' || $tipoEstudioNombre == 'Pago')){
                    $arrayInsert['FRACCION'] = $DBdato->Attribute_60;
                    // $arrayInsert['MANZANA'] = ucwords(strtolower($DBdato->Attribute_59));
                }
                else if ($tipoEstudio == 2 && $tipoEstudioNombre == 'Reclamos'){
                    $arrayInsert['FRACCION'] = $DBdato->Attribute_53;
                    // $arrayInsert['MANZANA'] = ucwords(strtolower($DBdato->Attribute_59));
                }


                $arrayInsert['estudio'] = $codEstudio;
                $arrayInsert['fecha_medicion'] = $tipoEstudio == 1 ? $DBdato->fechafin : $DBdato->submitdate;
                $arrayInsert['hora_medicion'] = $tipoEstudio == 1 ? $DBdato->horafin : $DBdato->submitdate;

                $arrayDBdatos = (array) $DBdato;
                // dd($arrayDBdatos);
                // dd($diccionarioPreguntas);
                foreach ($arrayDBdatos as $key=>$value){
                    if ($key == 'fecha_medicion' || $key == 'hora_medicion' || $key == 'registro' || $key == 'fechafin' || $key == 'horafin' || $key == 'submitdate' || $key == 'P1' || 
                        $key == 'P2_1' || $key == 'P2_2' || $key == 'P2_3' || $key == 'P2_4' || $key == 'P2_5' || $key == 'P2_6' || $key == 'P14' || $key == 'P15' ||
                        $key == 'CMPN_CDG'  || $key == 'PRECIO_PRODUCTO' || $key == 'PORC_PAGADO_PROD'
                        ){
                            $arrayInsert[$diccionarioPreguntas[$key]] = $value;
                        }
                    else if($key == 'token'){
                        $arrayInsert['token'] = $value;
                        // dd($DBdato);
                    }
                    

                    else {
                        
                        $arrayInsert[$diccionarioPreguntas[$key]] = "$value";
                    }
                }

                $existeRegistro = 0;
                if ($tipoEstudio == 1) {
                    $existeRegistro = DatoConsolidado::where('estudio',  $arrayInsert['estudio'])
                        ->where('registro', $arrayInsert['registro'])
                        ->count();
                    // dd($d);
                }
                else if ($tipoEstudio == 2) {
                    $existeRegistro = DatoConsolidado::where('estudio',  $arrayInsert['estudio'])
                        ->where('token', $arrayInsert['token'])
                        ->count();
                }

                if ($existeRegistro == 0){
                    $datoConsolidados = DatoConsolidado::create($arrayInsert);
                    // dd($datoConsolidados);
                }                
            }

            



            // $campoOrigenes = 

        }

        return "fin";
        
    }
}
