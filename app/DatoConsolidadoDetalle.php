<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatoConsolidadoDetalle extends Model
{
    protected $fillable = ['dato_consolidado_id', 'pregunta', 'respuesta'];

    public function DatoConsolidado(){
        return $this->belongsTo(DatoConsolidado::class);
    }
}
