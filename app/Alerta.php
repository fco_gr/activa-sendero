<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alerta extends Model
{
    protected $fillable = ['dato_consolidado_id', 'user_id', 'email', 'mensaje'];

    public function dato_consolidado(){
        return $this->belongsTo(DatoConsolidado::class);
    }


}
