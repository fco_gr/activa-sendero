<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CopiaPreguntasCodificadasCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datos:abiertas_web';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = app()->make('App\Http\Controllers\CopiaPreguntasCodificadasController');
        app()->call([$controller, 'index']);
    }
}
