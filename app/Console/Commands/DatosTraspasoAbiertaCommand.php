<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatosTraspasoAbiertaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datos:traspaso_abierta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '4 Copia los datos a la tabla para codificación';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = app()->make('App\Http\Controllers\TraspasoPreguntasAbiertasController');
        app()->call([$controller, 'traspaso']);
    }
}
