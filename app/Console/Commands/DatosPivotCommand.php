<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatosPivotCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datos:pivot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '2 Pivotea los datos consolidados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = app()->make('App\Http\Controllers\PivotController');
        app()->call([$controller, 'index']);
    }
}
