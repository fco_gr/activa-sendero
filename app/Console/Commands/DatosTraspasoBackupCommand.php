<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatosTraspasoBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datos:traspaso_backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '3 Recupera los datos de Doit y Lime, genera backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = app()->make('App\Http\Controllers\TraspasoBackupController');
        app()->call([$controller, 'index']);
    }
}
