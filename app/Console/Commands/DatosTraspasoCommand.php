<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatosTraspasoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datos:traspaso';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '1 Recupera los datos de Doit y Lime';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = app()->make('App\Http\Controllers\TraspasoController');
        app()->call([$controller, 'index']);
    }
}
