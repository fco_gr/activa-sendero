<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AlertaMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "Alerta por evaluación con insatisfacción – Estudio Tracking Experiencia Sendero";
    public $contacto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contacto)
    {
        $this->contacto = $contacto;
        // dd($this->contacto);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.alerta')
            ->from('alertas@activasite.com', 'Alerta')
            ->replyTo(Auth::user()->email, Auth::user()->name);
    }
}
