<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /**
         * Defining the user Roles
         */
        Gate::define('isAdmin', function ($user) {
            // if ($user->isAdmin()) {
            //     return true;
            // }

            // for simplicity
            return $user->type === 'admin';
        });

        Gate::define('isUser', function ($user) {
            // if ($user->isUser()) {
            //     return true;
            // }

            // for simplicity
            return $user->type === 'user';
        });

        Gate::define('isCodificador', function ($user) {
            return $user->type === 'codificador';
        });

        Gate::define('isAdminOrUser', function ($user) {
            return $user->type === 'admin' || $user->type === 'user';
        });

        Gate::define('isAdminOrUserOrAlerta', function ($user) {
            return $user->type === 'admin' || $user->type === 'user' || $user->type === 'alerta';
        });

        Gate::define('isAdminOrCodificador', function ($user) {
            return $user->type === 'admin' || $user->type === 'codificador';
        });

        Gate::define('isAdminOrAlerta', function ($user) {
            return $user->type === 'admin' || $user->type === 'alerta';
        });

        Passport::routes();
    }
}
