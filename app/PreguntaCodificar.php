<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreguntaCodificar extends Model
{
    public function dato_consolidado(){
        return $this->hasOne(DatoConsolidado::class);
    }
}
