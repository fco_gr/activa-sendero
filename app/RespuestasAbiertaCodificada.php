<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestasAbiertaCodificada extends Model
{

    protected $fillable = ['respuestas_abierta_id', 'pregunta_codificar_id', 'libro_codigo_id', 'user_id'];

    public function respuesta_abierta(){
        return $this->belongsTo(RespuestasAbierta::class);
    }

    

    
}
