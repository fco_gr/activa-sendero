<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatoConsolidado extends Model
{
    protected $fillable = [
        'id', 'estudio_id', 'fecha_medicion',  'hora_medicion', 'tipo_estudio', 'estudio', 'registro', 
        'fechafin', 'horafin', 'token', 'submitdate', 'P1', 'P1_1', 'P2_1', 'P2_2', 
        'P2_3', 'P2_4', 'P2_5', 'P2_6', 'P14', 'P14_1', 'P15', 'P16', 'P17', 
        'FCH_PROCESO', 'FCH_SERVICIO', 'FCH_VENTA', 'DIAS_DIF_VTA_SERV', 'RUT_CLNT_TIT', 
        'NMB_CLNT_TIT', 'COD_AREA_FONO_FIJO', 'FONO_FIJO', 'FONO_CELULAR', 'EMAIL_CLIE', 'IND_EMPL_SENDERO', 
        'CMPN_CDG', 'PRQS_CDG', 'CNTR_CDG_SERIE', 'CNTR_NMR', 'PRECIO_PRODUCTO', 'CDG_PRODUCTO', 
        'DSC_PROMOCION', 'TIPO_PRODUCTO', 'NMB_PRODUCTO', 'TIPO_NECES_OP', 'TIPO_ORIGEN_VENTA', 
        'PORC_PAGADO_PROD', 'FCH_NOTIF', 'MARCA_ANG_NOTIF', 'FCH_MPU_MV', 'POSEE_MPU_MV', 
        'MARCA_MPUMV_NOTIF', 'PARQUE_FISICO', 'ZONA_PARQUE', 'RUT_SPV', 'NMB_SPV', 
        'NMB_SPV_REAL', 'VNDD_RUT', 'NMB_VNDD', 'GERENCIA_VENTA', 'ZONA_SPV', 
        'FUNERARIA', 'FCH_INICIO_SERV', 'FCH_FIN_SERV', 'IND_USO_PRODUCTO', 'IND_TIPO_PAGO', 
        'CANAL_PAGO_PROD', 'CANAL_PAGO_MNTN', 'IND_RECLAMOS', 'MAX_FCH_CIERRE_RCLM_SLCT', 'USUARIO_NOTIFICADOR', 
        'NOTIF_CONTACT_CENTER', 'PARENTESCO', 'IND_TIPO_CONTRATO', 'IND_COLUMBARIO', 'CNTR_IND_ESTADO', 'MAX_FCH_ENTRE_CR', 
        'MAX_FCH_SERVICIO_SEP', 'TIPO_SUB_PRODUCTO', 'MUESTRA_P7', 'MUESTRA_P8', 'ORIGEN_BD', 'created_at', 'updated_at',

        'ACOMP_P5_1', 'ACOMP_P5_2', 'ACOMP_P5_3', 'ACOMP_P5_4', 'ACOMP_P5_5', 'ACOMP_P5_6', 'ACOMP_P5_7', 'ACOMP_P5_8', 'ACOMP_P8', 
        'ACOMP_P9_1', 'ACOMP_P9_2', 'ACOMP_P9_3', 'ACOMP_P9_4', 'ACOMP_P9_5', 'ACOMP_P9_6', 'ACOMP_P10_1', 'ACOMP_P10_2', 'ACOMP_P10_3', 
        'ACOMP_P10_4', 'ACOMP_P10_5', 'ACOMP_P10_6', 'ACOMP_P10_7', 'ACOMP_P10_8', 'ACOMP_P11_1', 'ACOMP_P11_2', 'ACOMP_P11_3', 
        'ACOMP_P11_4', 'ACOMP_P11_5', 'ACOMP_P11_6', 'ACOMP_P12', 'ACOMP_P13', 'VENTA_P8_1', 'VENTA_P8_2', 
        'VENTA_P8_3', 'VENTA_P8_4', 'VENTA_P8_5', 'VENTA_P8_6', 'VENTA_P8_7', 'VENTA_P8_8', 'VENTA_P9', 
        
        'PAGO_P6_1', 'PAGO_P6_2', 'PAGO_P6_3', 'PAGO_P6_4', 'PAGO_P6_5', 'PAGO_P6_6', 'PAGO_P6_7', 'PAGO_P6_8', 
        'PAGO_P8_1', 'PAGO_P8_2', 'PAGO_P8_3', 'PAGO_P8_4', 'PAGO_P8_5', 'PAGO_P8_6', 
        'PAGO_P11_1', 'PAGO_P11_2', 'PAGO_P11_3', 'PAGO_P11_4', 
        
        'RECLA_P6_1', 'RECLA_P6_2', 'RECLA_P6_3', 'RECLA_P6_4', 'RECLA_P6_5', 'RECLA_P6_6', 
        'RECLA_P8', 
        'RECLA_P11_1', 'RECLA_P11_2', 'RECLA_P11_3', 'RECLA_P11_4',

        'VISITA_PAGO_P2', 'VISITA_PAGO_P5', 'VISITA_PAGO_P7', 'VISITA_PAGO_P7_OTRO', 'VISITA_PAGO_P9', 'VISITA_PAGO_P10_1', 
        'VISITA_PAGO_P10_2', 'VISITA_PAGO_P10_3', 'VISITA_PAGO_P10_4', 'VISITA_PAGO_P10_OTRO', 'VISITA_PAGO_P11_1', 'VISITA_PAGO_P16', 
        'VISITA_PAGO_TIPO', 'INFO_P9', 'INFO_P10_1', 'INFO_P10_2', 'INFO_P10_3', 'INFO_P10_4', 'INFO_P10_6', 'INFO_P11_1', 'INFO_P11_2', 
        'INFO_P11_3', 'INFO_P11_8',

        'FRACCION', 'MANZANA'
    ];


    public function DatoConsolidadoDetalles(){
        return $this->hasMany(DatoConsolidadoDetalle::class);
    }

    public function estudio(){
        return $this->belongsTo(Estudio::class);
    }

    public function respuestas_abiertas(){
        return $this->hasOne(RespuestasAbierta::class);
    }

    public function alertas(){
        return $this->hasMany(Alerta::class);
    }


}
