<?php

use Carbon\Carbon;
use Illuminate\Http\Request;


if (!function_exists('aplicaFiltros')) {
    function aplicaFiltros(Request $request)
    {
        // dd($request->all());
        if ($request->all() == []){
            return '';
        }
            


        $viajes = $request->filtro ? $request->filtro["viaje_id"] : [];
        $familias = $request->filtro ? $request->filtro["familia_id"] : [];
        $tipoProductos = $request->filtro ? $request->filtro["tipo_producto_id"] : [];
        $funerarias = $request->filtro ? $request->filtro["funeraria_id"] : [];
        $necesidades = $request->filtro ? $request->filtro["necesidad_id"] : [];
        $parques = $request->filtro ? $request->filtro["parque_id"] : [];
        $fecha_ini = $request->filtro && $request->filtro["fecha_ini"] != null ? Carbon::parse($request->filtro["fecha_ini"])->format('Y-m-d') : null;
        $fecha_fin = $request->filtro && $request->filtro["fecha_fin"] != null ? Carbon::parse($request->filtro["fecha_fin"])->format('Y-m-d') : null;
        
        $origen_ventas = $request->filtro ? $request->filtro["origen_venta_id"] : [];
        $uso_productos = $request->filtro ? $request->filtro["uso_producto_id"] : [];

        $sWhere = "";
        
        if (count($viajes) > 0){
            
            $claves = implode("','", $viajes);
            $sWhere .= " and tipo_estudio in ('$claves') ";
            // dd($sWhere);
        }

        if (count($tipoProductos) > 0){
            $claves = implode("','", $tipoProductos);
            $sWhere .= " and TIPO_SUB_PRODUCTO in ('$claves') ";
        }
        else if (count($familias) > 0){
            // dd($familia);
            $consulta = "";
            if (in_array("Sepultura", $familias)){
                $consulta .= " TIPO_SUB_PRODUCTO = 'SEPULTURA' OR ";
                $consulta .= " TIPO_SUB_PRODUCTO = 'PLAN DE SEPULTURA' OR ";
                $consulta .= " TIPO_SUB_PRODUCTO = 'JARDIN' ";
            }
            if (in_array("Cremaciones", $familias)){
                $consulta .= $consulta != '' ? " OR " : "";
                $consulta .= " TIPO_SUB_PRODUCTO = 'CREMACION' OR ";
                $consulta .= " TIPO_SUB_PRODUCTO = 'PLAN DE CREMACION' ";
            }
            if (in_array("Columbarios", $familias)){
                $consulta .= $consulta != '' ? " OR " : "";
                $consulta .= " TIPO_SUB_PRODUCTO = 'COLUMBARIO' OR ";
                $consulta .= " TIPO_SUB_PRODUCTO = 'PLAN DE COLUMBARIO'  ";
            }

            $sWhere .= " and ($consulta) ";

            
        }

        if (count($funerarias) > 0){

            // dd(in_array("IVAN MARTINEZ", $funerarias));
            $consulta = "";
            if (in_array("PARQUE DEL SENDERO", $funerarias)){
                $consulta .= " FUNERARIA LIKE '%PARQUE DEL SENDERO%' ";
            }
            if (in_array("IVAN MARTINEZ", $funerarias)){
                $consulta .= $consulta != "" ? " or " : "";
                $consulta .= " FUNERARIA LIKE '%IVAN MARTINEZ%' ";
            }
            if (in_array("OTRAS", $funerarias)){
                $consulta .= $consulta != "" ? " or " : "";
                $consulta .= " (FUNERARIA NOT LIKE '%PARQUE DEL SENDERO%' AND FUNERARIA NOT LIKE '%IVAN MARTINEZ%') ";
            }

            // dd($consulta);
            
            $sWhere .= " and ($consulta) and FUNERARIA <> ''";
        }

        if (count($necesidades) > 0){
            $claves = implode("','", $necesidades);
            $sWhere .= " and TIPO_NECES_OP in ('$claves') ";
        }

        if (count($parques) > 0){
            $claves = implode("','", $parques);
            $sWhere .= " and PARQUE_FISICO in ('$claves') ";
        }

        if ($fecha_ini && $fecha_fin){
            $sWhere .= " and fecha_medicion >= '$fecha_ini' and fecha_medicion <= '$fecha_fin' ";
        }

        if (count($origen_ventas) > 0){
            
            $claves = implode("','", $origen_ventas);
            $sWhere .= " and tipo_origen_venta in ('$claves') ";
            // dd($sWhere);
        }

        if (count($uso_productos) > 0){
            
            $claves = implode("','", $uso_productos);
            $sWhere .= " and IND_USO_PRODUCTO in ('$claves') ";
            // dd($sWhere);
        }
            

        // dd($sWhere);
        return $sWhere;


    }
}