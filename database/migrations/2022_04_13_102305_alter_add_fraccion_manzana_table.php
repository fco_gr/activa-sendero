<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAddFraccionManzanaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dato_consolidados', function (Blueprint $table) {
            $table->string('FRACCION')->nullable()->after('ORIGEN_BD');
            $table->string('MANZANA')->nullable()->after('ORIGEN_BD');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dato_consolidados', function (Blueprint $table) {
            $table->dropColumn('FRACCION');
            $table->dropColumn('MANZANA');
        });
    }
}
