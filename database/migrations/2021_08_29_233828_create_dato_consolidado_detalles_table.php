<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatoConsolidadoDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dato_consolidado_detalles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('dato_consolidado_id')->constrained();
            $table->string('pregunta', 20);
            $table->integer('respuesta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dato_consolidado_detalles');
    }
}
