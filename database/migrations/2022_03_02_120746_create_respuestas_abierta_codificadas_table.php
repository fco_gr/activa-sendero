<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespuestasAbiertaCodificadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_abierta_codificadas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('respuestas_abierta_id')->constrained();
            $table->foreignId('pregunta_codificar_id')->constrained();
            $table->foreignId('libro_codigo_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas_abierta_codificadas');
    }
}
