<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios', function (Blueprint $table) {
            $table->id();
            $table->integer('id_estudio')->nullable();
            $table->integer('sid')->nullable();
            $table->integer('cod_estudio');
            $table->string('nom_estudio', 100);
            $table->string('nom_estudio_completo', 100);
            $table->foreignId('tipo_estudio_id')->constrained();
            $table->string('tabla', 100)->nullable();
            $table->string('tabla_survey', 100)->nullable();
            $table->string('tabla_token', 100)->nullable();
            $table->boolean('sincroniza')->default(true);
            $table->integer('orden')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios');
    }
}
