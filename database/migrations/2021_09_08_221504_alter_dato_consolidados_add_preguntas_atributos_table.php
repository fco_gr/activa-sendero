<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDatoConsolidadosAddPreguntasAtributosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dato_consolidados', function (Blueprint $table) {
            $table->string('ACOMP_P5_1', 3)->nullable();
            $table->string('ACOMP_P5_2', 3)->nullable();
            $table->string('ACOMP_P5_3', 3)->nullable();
            $table->string('ACOMP_P5_4', 3)->nullable();
            $table->string('ACOMP_P5_5', 3)->nullable();
            $table->string('ACOMP_P5_6', 3)->nullable();
            $table->string('ACOMP_P5_7', 3)->nullable();
            $table->string('ACOMP_P5_8', 3)->nullable();
            $table->string('ACOMP_P8', 3)->nullable();
            $table->string('ACOMP_P9_1', 3)->nullable();
            $table->string('ACOMP_P9_2', 3)->nullable();
            $table->string('ACOMP_P9_3', 3)->nullable();
            $table->string('ACOMP_P9_4', 3)->nullable();
            $table->string('ACOMP_P9_5', 3)->nullable();
            $table->string('ACOMP_P9_6', 3)->nullable();
            $table->string('ACOMP_P10_1', 3)->nullable();
            $table->string('ACOMP_P10_2', 3)->nullable();
            $table->string('ACOMP_P10_3', 3)->nullable();
            $table->string('ACOMP_P10_4', 3)->nullable();
            $table->string('ACOMP_P10_5', 3)->nullable();
            $table->string('ACOMP_P10_6', 3)->nullable();
            $table->string('ACOMP_P10_7', 3)->nullable();
            $table->string('ACOMP_P10_8', 3)->nullable();
            $table->string('ACOMP_P11_1', 3)->nullable();
            $table->string('ACOMP_P11_2', 3)->nullable();
            $table->string('ACOMP_P11_3', 3)->nullable();
            $table->string('ACOMP_P11_4', 3)->nullable();
            $table->string('ACOMP_P11_5', 3)->nullable();
            $table->string('ACOMP_P11_6', 3)->nullable();
            $table->string('ACOMP_P12', 3)->nullable();
            $table->string('ACOMP_P13', 3)->nullable();
            
            $table->string('VENTA_P8_1', 3)->nullable();
            $table->string('VENTA_P8_2', 3)->nullable();
            $table->string('VENTA_P8_3', 3)->nullable();
            $table->string('VENTA_P8_4', 3)->nullable();
            $table->string('VENTA_P8_5', 3)->nullable();
            $table->string('VENTA_P8_6', 3)->nullable();
            $table->string('VENTA_P8_7', 3)->nullable();
            $table->string('VENTA_P8_8', 3)->nullable();
            $table->string('VENTA_P9', 3)->nullable();


            $table->string('PAGO_P6_1', 3)->nullable();
            $table->string('PAGO_P6_2', 3)->nullable();
            $table->string('PAGO_P6_3', 3)->nullable();
            $table->string('PAGO_P6_4', 3)->nullable();
            $table->string('PAGO_P6_5', 3)->nullable();
            $table->string('PAGO_P6_6', 3)->nullable();
            $table->string('PAGO_P6_7', 3)->nullable();
            $table->string('PAGO_P6_8', 3)->nullable();
            $table->string('PAGO_P8_1', 3)->nullable();
            $table->string('PAGO_P8_2', 3)->nullable();
            $table->string('PAGO_P8_3', 3)->nullable();
            $table->string('PAGO_P8_4', 3)->nullable();
            $table->string('PAGO_P8_5', 3)->nullable();
            $table->string('PAGO_P8_6', 3)->nullable();
            $table->string('PAGO_P11_1', 3)->nullable();
            $table->string('PAGO_P11_2', 3)->nullable();
            $table->string('PAGO_P11_3', 3)->nullable();
            $table->string('PAGO_P11_4', 3)->nullable();

            $table->string('RECLA_P6_1', 3)->nullable();
            $table->string('RECLA_P6_2', 3)->nullable();
            $table->string('RECLA_P6_3', 3)->nullable();
            $table->string('RECLA_P6_4', 3)->nullable();
            $table->string('RECLA_P6_5', 3)->nullable();
            $table->string('RECLA_P6_6', 3)->nullable();
            $table->string('RECLA_P8', 3)->nullable();
            $table->string('RECLA_P11_1', 3)->nullable();
            $table->string('RECLA_P11_2', 3)->nullable();
            $table->string('RECLA_P11_3', 3)->nullable();
            $table->string('RECLA_P11_4', 3)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dato_consolidados', function (Blueprint $table) {
            $table->dropColumn('ACOMP_P5_1');
            $table->dropColumn('ACOMP_P5_2');
            $table->dropColumn('ACOMP_P5_3');
            $table->dropColumn('ACOMP_P5_4');
            $table->dropColumn('ACOMP_P5_5');
            $table->dropColumn('ACOMP_P5_6');
            $table->dropColumn('ACOMP_P5_7');
            $table->dropColumn('ACOMP_P5_8');
            $table->dropColumn('ACOMP_P8');
            $table->dropColumn('ACOMP_P9_1');
            $table->dropColumn('ACOMP_P9_2');
            $table->dropColumn('ACOMP_P9_3');
            $table->dropColumn('ACOMP_P9_4');
            $table->dropColumn('ACOMP_P9_5');
            $table->dropColumn('ACOMP_P9_6');
            $table->dropColumn('ACOMP_P10_1');
            $table->dropColumn('ACOMP_P10_2');
            $table->dropColumn('ACOMP_P10_3');
            $table->dropColumn('ACOMP_P10_4');
            $table->dropColumn('ACOMP_P10_5');
            $table->dropColumn('ACOMP_P10_6');
            $table->dropColumn('ACOMP_P10_7');
            $table->dropColumn('ACOMP_P10_8');
            $table->dropColumn('ACOMP_P11_1');
            $table->dropColumn('ACOMP_P11_2');
            $table->dropColumn('ACOMP_P11_3');
            $table->dropColumn('ACOMP_P11_4');
            $table->dropColumn('ACOMP_P11_5');
            $table->dropColumn('ACOMP_P11_6');
            $table->dropColumn('ACOMP_P12');
            $table->dropColumn('ACOMP_P13');

            $table->dropColumn('VENTA_P8_1');
            $table->dropColumn('VENTA_P8_2');
            $table->dropColumn('VENTA_P8_3');
            $table->dropColumn('VENTA_P8_4');
            $table->dropColumn('VENTA_P8_5');
            $table->dropColumn('VENTA_P8_6');
            $table->dropColumn('VENTA_P8_7');
            $table->dropColumn('VENTA_P8_8');
            $table->dropColumn('VENTA_P9');


            $table->dropColumn('PAGO_P6_1');
            $table->dropColumn('PAGO_P6_2');
            $table->dropColumn('PAGO_P6_3');
            $table->dropColumn('PAGO_P6_4');
            $table->dropColumn('PAGO_P6_5');
            $table->dropColumn('PAGO_P6_6');
            $table->dropColumn('PAGO_P6_7');
            $table->dropColumn('PAGO_P6_8');
            $table->dropColumn('PAGO_P8_1');
            $table->dropColumn('PAGO_P8_2');
            $table->dropColumn('PAGO_P8_3');
            $table->dropColumn('PAGO_P8_4');
            $table->dropColumn('PAGO_P8_5');
            $table->dropColumn('PAGO_P8_6');
            $table->dropColumn('PAGO_P11_1');
            $table->dropColumn('PAGO_P11_2');
            $table->dropColumn('PAGO_P11_3');
            $table->dropColumn('PAGO_P11_4');

            $table->dropColumn('RECLA_P6_1');
            $table->dropColumn('RECLA_P6_2');
            $table->dropColumn('RECLA_P6_3');
            $table->dropColumn('RECLA_P6_4');
            $table->dropColumn('RECLA_P6_5');
            $table->dropColumn('RECLA_P6_6');
            $table->dropColumn('RECLA_P8');
            $table->dropColumn('RECLA_P11_1');
            $table->dropColumn('RECLA_P11_2');
            $table->dropColumn('RECLA_P11_3');
            $table->dropColumn('RECLA_P11_4');
        });
    }
}
