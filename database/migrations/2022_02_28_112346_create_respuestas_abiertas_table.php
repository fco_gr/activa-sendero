<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespuestasAbiertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_abiertas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('estudio_id')->constrained();
            $table->foreignId('dato_consolidado_id')->constrained();

            $table->string('estudio', 20)->nullable();
            $table->unsignedInteger('registro')->nullable();

            $table->unsignedInteger('lime_id')->nullable();
            $table->string('token', 35)->nullable();

            $table->text('col_1')->nullable();
            $table->text('col_2')->nullable();
            $table->text('col_3')->nullable();
            $table->text('col_4')->nullable();
            $table->text('col_5')->nullable();
            $table->text('col_6')->nullable();
            $table->text('col_7')->nullable();
            $table->text('col_8')->nullable();
            $table->text('col_9')->nullable();
            $table->text('col_10')->nullable();
            $table->text('col_11')->nullable();

            $table->boolean('status_col_1')->default(false);
            $table->boolean('status_col_2')->default(false);
            $table->boolean('status_col_3')->default(false);
            $table->boolean('status_col_4')->default(false);
            $table->boolean('status_col_5')->default(false);
            $table->boolean('status_col_6')->default(false);
            $table->boolean('status_col_7')->default(false);
            $table->boolean('status_col_8')->default(false);
            $table->boolean('status_col_9')->default(false);
            $table->boolean('status_col_10')->default(false);
            $table->boolean('status_col_11')->default(false);

            $table->boolean('codificado_col_1')->default(false);
            $table->boolean('codificado_col_2')->default(false);
            $table->boolean('codificado_col_3')->default(false);
            $table->boolean('codificado_col_4')->default(false);
            $table->boolean('codificado_col_5')->default(false);
            $table->boolean('codificado_col_6')->default(false);
            $table->boolean('codificado_col_7')->default(false);
            $table->boolean('codificado_col_8')->default(false);
            $table->boolean('codificado_col_9')->default(false);
            $table->boolean('codificado_col_10')->default(false);
            $table->boolean('codificado_col_11')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas_abiertas');
    }
}
