<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatoConsolidadoAbiertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dato_consolidado_abiertas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('estudio_id')->constrained();
            $table->string('pregunta', 20);
            $table->foreignId('dato_consolidado_id')->constrained();
            $table->integer('registro')->nullable();
            $table->string('token')->nullable();
            $table->integer('lime_id')->nullable();
            $table->integer('codigo');

            $table->unique(['estudio_id', 'pregunta', 'dato_consolidado_id', 'token', 'lime_id', 'codigo'], 'estudio_preg_consolida_libro_id_unique');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dato_consolidado_abiertas');
    }
}
