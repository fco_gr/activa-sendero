<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatoConsolidadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dato_consolidados', function (Blueprint $table) {
            $table->id();
            $table->foreignId('estudio_id')->constrained();
            $table->date('fecha_medicion')->nullable();
            $table->time('hora_medicion')->nullable();
            $table->string('tipo_estudio', 100)->nullable();
            $table->string('estudio', 50)->nullable();
            
            $table->integer('registro')->nullable();
            $table->date('fechafin')->nullable();
            $table->time('horafin')->nullable();

            $table->string('token', 20)->nullable();
            $table->datetime('submitdate')->nullable();
            $table->integer('P1');
            $table->string('P1_1', 1500);
            $table->integer('P2_1');
            $table->integer('P2_2');
            $table->integer('P2_3');
            $table->integer('P2_4');
            $table->integer('P2_5');
            $table->integer('P2_6');
            $table->integer('P14')->nullable();
            $table->string('P14_1', 1500);
            $table->integer('P15')->nullable();
            $table->string('P16', 2000)->nullable();
            $table->string('P17', 100)->nullable();
            $table->string('FCH_PROCESO')->nullable();
            $table->string('FCH_SERVICIO')->nullable();
            $table->string('FCH_VENTA')->nullable();
            $table->string('DIAS_DIF_VTA_SERV')->nullable();
            $table->string('RUT_CLNT_TIT', 20)->nullable();
            $table->string('NMB_CLNT_TIT', 100)->nullable();
            $table->string('COD_AREA_FONO_FIJO')->nullable();
            $table->string('FONO_FIJO', 20)->nullable();
            $table->string('FONO_CELULAR', 20)->nullable();
            $table->string('EMAIL_CLIE', 100)->nullable();
            $table->string('IND_EMPL_SENDERO', 100)->nullable();
            $table->integer('CMPN_CDG')->nullable();
            $table->string('PRQS_CDG')->nullable();
            $table->string('CNTR_CDG_SERIE', 20)->nullable();
            $table->string('CNTR_NMR')->nullable();
            $table->string('PRECIO_PRODUCTO', 10)->nullable();
            $table->string('CDG_PRODUCTO', 50)->nullable();
            $table->string('DSC_PROMOCION', 50)->nullable();
            $table->string('TIPO_PRODUCTO', 50)->nullable();
            $table->string('NMB_PRODUCTO', 50)->nullable();
            $table->string('TIPO_NECES_OP', 50)->nullable();
            $table->string('TIPO_ORIGEN_VENTA', 50)->nullable();
            $table->integer('PORC_PAGADO_PROD')->nullable();
            $table->string('FCH_NOTIF', 50)->nullable();
            $table->string('MARCA_ANG_NOTIF', 50)->nullable();
            $table->string('FCH_MPU_MV', 20)->nullable();
            $table->string('POSEE_MPU_MV', 50)->nullable();
            $table->string('MARCA_MPUMV_NOTIF', 50)->nullable();
            $table->string('PARQUE_FISICO', 50)->nullable();
            $table->string('ZONA_PARQUE', 50)->nullable();
            $table->string('RUT_SPV', 20)->nullable();
            $table->string('NMB_SPV', 100)->nullable();
            $table->string('NMB_SPV_REAL', 100)->nullable();
            $table->string('VNDD_RUT', 20)->nullable();
            $table->string('NMB_VNDD', 100)->nullable();
            $table->string('GERENCIA_VENTA', 50)->nullable();
            $table->string('ZONA_SPV', 50)->nullable();
            $table->string('FUNERARIA', 100)->nullable();
            $table->string('FCH_INICIO_SERV', 50)->nullable();
            $table->string('FCH_FIN_SERV', 50)->nullable();
            $table->string('IND_USO_PRODUCTO', 50)->nullable();
            $table->string('IND_TIPO_PAGO', 50)->nullable();
            $table->string('CANAL_PAGO_PROD', 50)->nullable();
            $table->string('CANAL_PAGO_MNTN', 50)->nullable();
            $table->string('IND_RECLAMOS', 50)->nullable();
            $table->string('MAX_FCH_CIERRE_RCLM_SLCT')->nullable();
            $table->string('USUARIO_NOTIFICADOR', 50)->nullable();
            $table->string('NOTIF_CONTACT_CENTER', 50)->nullable();
            $table->string('PARENTESCO', 50)->nullable();
            $table->string('IND_TIPO_CONTRATO', 50)->nullable();
            $table->string('IND_COLUMBARIO', 50)->nullable();
            $table->string('CNTR_IND_ESTADO', 50)->nullable();
            $table->string('MAX_FCH_ENTRE_CR')->nullable();
            $table->string('MAX_FCH_SERVICIO_SEP')->nullable();
            $table->string('TIPO_SUB_PRODUCTO', 50)->nullable();
            $table->string('MUESTRA_P7')->nullable();
            $table->string('MUESTRA_P8')->nullable();
            $table->string('ORIGEN_BD', 150)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dato_consolidados');
    }
}
