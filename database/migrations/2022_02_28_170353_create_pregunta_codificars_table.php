<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntaCodificarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_codificars', function (Blueprint $table) {
            $table->id();
            $table->string('texto');

            $table->string('venta_tipo')->nullable();
            $table->string('venta_cuestionario')->nullable();
            $table->string('venta_campo')->nullable();

            $table->string('acomp_tipo')->nullable();
            $table->string('acomp_cuestionario')->nullable();
            $table->string('acomp_campo')->nullable();

            $table->string('reclamo_tipo')->nullable();
            $table->string('reclamo_cuestionario')->nullable();
            $table->string('reclamo_campo')->nullable();

            $table->string('visita_pago_tipo')->nullable();
            $table->string('visita_pago_cuestionario')->nullable();
            $table->string('visita_pago_campo')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_codificars');
    }
}
