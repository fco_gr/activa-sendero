<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntaLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_libros', function (Blueprint $table) {
            $table->id();
            $table->string('pregunta');
            $table->foreignId('categoria_pregunta_one_id')->nullable()->constrained();
            $table->foreignId('categoria_pregunta_two_id')->nullable()->constrained();
            $table->integer('codigo');
            $table->string('descripcion');
            $table->index(['pregunta', 'codigo']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_libros');
    }
}
